<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>


<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagExin04">
    <div class="pagExin04--bg">
        <img src="images/psgEx4-12.svg" alt="" class="pagExin04--bg01">
        <img src="images/psgEx4-14.svg" alt="" class="pagExin04--bg02">
        <img src="images/psgEx4-13.svg" alt="" class="pagExin04--bg03">
        <img src="images/psgEx4-12.svg" alt="" class="pagExin04--bg04">
    </div>
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin04-bannerBk">
        <div class="pagExin04-pageTitBk">
            <h1 class="">
                Your Lifelong <br> Learning Partner​
            </h1>
        </div>
        <img src="images/psgEx4-02.png" alt="" class="pagExin04-banner">
    </div>
    
    <div class="pagExin04-contentBk">
        <div class="max_width">
            <!-- <div class="pagExin04-sectionBk pagExin04-sectionBk--01">
                <img src="images/psgEx4-15.png" alt="" class="img01">
                <img src="images/psgEx4-30.png" alt="" class="img02">
                <img src="images/psgEx4-33.png" alt="" class="img03">
                <img src="images/psgEx4-34.png" alt="" class="img04">
            </div> -->
            <div class="pagExin04-sectionBk pagExin04-sectionBk--02">
                <h3 class="pagExin04-sectionTit">
                    Lifelong Learning
                </h3>
                <p class="pt-25 typo-black">
                    Promoting "lifelong learning" is the original intent of the LTTC. We’ve accompanied many of our learners through different stages in life—from school and job searches to employment and advanced studies. LTTC students and test takers come from all corners of Taiwan, each with a different identity. From schoolchildren and teenagers to adults and active seniors, the one thing they have in common is that they all discover the joy and accomplishment of learning here.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--03">
                <div class="tableBk01">
                    <img src="images/psgEx4-label01--sml.svg" alt="" class="label--sml" width="100%">
                    <img src="images/psgEx4-label01--big.svg" alt="" class="label--big mb-40" width="100%">
                    <img src="images/psgEx4-16.png" alt="" class="mt-40 mb-40" width="100%">
                    <p class="typo-black">
                        LTTC learners span all age groups, although most are over 18 years old. As Taiwan promotes the learning of English language and a second foreign language, the LTTC has launched competency oriented courses for middle-school students in recent years, which have expanded the age distribution of students.
                    </p>
                </div>
                <div class="tableBk01">
                    <img src="images/psgEx4-label02--sml.svg" alt="" class="label--sml" width="100%">
                    <img src="images/psgEx4-label02--big.svg" alt="" class="label--big mb-40" width="100%">
                    <img src="images/psgEx4-17.png" alt="" class="mt-40" width="100%">
                    <p class="typo-black table--right">
                        The learners are primarily business professionals, along with workers in the military, civil service, and education.
                    </p>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--04">
                <div class="img01">
                    <img src="images/psgEx4-18.png" alt="" class="mb-20" width="100%">
                </div>
                <p class="pt-25 typo-black">
                    In terms of the age distribution of test takers, the range for the Japanese-Language Proficiency Test (JLPT) is from 5 to 96 years old.
                    In addition, more people continue to set new goals to challenge themselves.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--05">
                <div class="label--sml">
                    <img src="images/psgEx4-label03--sml.svg" alt="" class="label--sml" width="100%">
                </div>
                <div class="label--big">
                    <img src="images/psgEx4-label03--big.svg" alt="" class="" width="100%">
                </div>
                <div class="tableBk01">
                    <p class="typo-black mt-20">
                        More than 1,000 people hold 4 certificates. <br><br>
                        More than 300,000 people hold 3 certificates. <br><br>
                        More than 210,000 people hold 2 certificates. 
                    </p>
                    <img src="images/psgEx4-19.png" alt="" class="mt-40 mb-40" width="100%">
                </div>
                <h3 class="pagExin04-sectionTit">
                    <!-- <span> -->
                        Learning Oriented Assessment
                    <!-- </span>  -->
                </h3>
                <p class="pt-25 typo-black">
                    In the past, the public's impression of exams boiled down to the final score. No matter how well the individual performed, the score was always the end of learning. The LTTC’s intent is to encourage learning for its own sake and to make test evaluation more forward-looking. The goal of assessment is to help students and teachers plan and effectively achieve the next stage of learning. It is for this reason that the center provides individualized diagnostic services in training and testing. The hope is to implement the concept of "Learning Oriented Assessment" to guide and cultivate self-directed learning. 
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--06">
                <h3 class="pagExin04-sectionTit">
                    <span>
                        “Dr. GEPT” Offers Individualized Feedback
                    </span> 
                </h3>
                <p class="pt-25 typo-black">
                    After the GEPT results are announced, test takers can log on to the “Dr. GEPT” to view their test performance. Individualized feedback includes analysis of strengths and weaknesses, discussion of unfamiliar sentence structures and vocabulary plus further learning suggestions. The “Dr. GEPT” has so far been used by more than 50,000 people within two months of its launch and achieved an average satisfaction rate of over 90%.  ​ 
                </p>
                <div class="imgBk">
                    <div class="img01">
                        <img src="images/psgEx4-20.png" alt="" class="" width="100%">
                    </div>
                    <div class="img02">
                        <img src="images/psgEx4-21.png" alt="" class="img--sml" width="100%">
                        <img src="images/psgEx4-31.png" alt="" class="img--ipad" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--07">
                <h3 class="pagExin04-sectionTit">
                    “GEPT Kids” Individualized Diagnosis and Analysis 
                </h3>
                <p class="pt-25 typo-black">
                    The test certificate provides 18 ability indicators. Individualized diagnosis and analysis can guide students in their self-directed learning.
                </p>
                <div class="imgBk">
                    <div class="img01">
                        <img src="images/psgEx4-03.png" alt="" class="" width="100%">
                    </div>
                    <div class="img02">
                        <img src="images/psgEx4-04.png" alt="" class="" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--08">
                <h3 class="pagExin04-sectionTit">
                    Individualized Diagnostic Analysis and Feedback in the LTTC Classes
                </h3>
                <p class="pt-25 typo-black">
                    In the LTTC writing class for example, the individualized diagnostic analysis and feedback are as follows：
                </p>
                <div class="imgBk">
                    <div class="imgListBk">
                        <p class="list">relevance & adequacy</p>
                        <p class="list">organization</p>
                        <p class="list">coherence</p>
                        <p class="list">grammatical use</p>
                        <p class="list">lexical use</p>
                    </div>
                    <div class="img01">
                        <img src="images/psgEx4-05.png" alt="" class="" width="100%">
                    </div>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--09">
                <h3 class="pagExin04-sectionTit">
                    <span>
                        A Driving Force of English Education in Taiwan​ 
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    Developed by the LTTC, the GEPT is the first English assessment in Taiwan to cover both receptive skills (listening and reading) and productive skills (speaking and writing). Compared to earlier assessments, which tested only reading and writing skills, the GEPT encourages learners to develop all four modes of communication (listening, reading, speaking and writing) in a balanced manner. This has resulted in a more well-rounded and positive approach to language education.
                </p>
                <div class="imgBk">  
                    <div class="img01 pb-40">
                        <img src="images/psgEx4-22.png" alt="" class="ptb-30" width="100%">
                        <p class="typo-black">
                            English education has firmly taken  root in Taiwan. The starting age of test takers has also dropped.
                        </p>
                    </div>
                    <div class="img02 pb-40">
                        <img src="images/psgEx4-23.png" alt="" class="ptb-30" width="100%">
                        <p class="typo-black">
                            Learners are putting more emphasis on speaking and writing. 80% of learners who have passed the listening & reading parts of the GEPT return within 2 years to complete the speaking & writing parts.
                        </p>
                    </div>
                </div>
                <div class="img03">
                    <img src="images/psgEx4-24.png" alt="" class="ptb-30" width="100%">
                    <p class="typo-black">
                        Learners’ listening and reading ability has  been improving yearly. Over the past 20 years, test takers have improved an average of 11 points in intermediate listening and 9 points in reading, and the pass rate has increased by more than 20%
                    </p>
                </div>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--10">
                <!-- <h3 class="label">
                    知識的循環與回饋
                    <br>
                    The Knowledge Feedback Loop 
                </h3> -->
                <div class="label--sml">
                    <img src="images/psgEx4-label04--sml.svg" alt="" class="label--sml" width="100%">
                </div>
                <div class="label--big">
                    <img src="images/psgEx4-label04--big.svg" alt="" class="" width="100%">
                </div>
                <div class="img01">
                    <img src="images/psgEx4-25.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-26.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-27.png" alt="" class="" width="100%">
                    <img src="images/psgEx4-28.png" alt="" class="" width="100%">
                </div>
                <div class="img02">
                    <img src="images/psgEx4-32.png" alt="" class="" width="100%">
                </div>
                <p class="typo-black">
                    In addition to its support for education of the general public, the LTTC is also committed to improving the skills of English teachers. The center regularly organizes teaching and assessment-related curriculum-based events, working shoulder-to-shoulder with English educators to share accumulated assessments, teaching results and experience.
                </p>
            </div>
            <div class="pagExin04-sectionBk pagExin04-sectionBk--11">
                <div class="img01">
                    <img src="images/psgEx4-06.png" alt="" class="" width="">
                    <p class="pt-15 typo-black">
                        The convener of the twelve-year public education, Professor Vincent W. Chang gives a lecture on Competency Oriented Assessment.
                    </p>
                </div>
                <div class="img02">
                    <img src="images/psgEx4-07.png" alt="" class="" width="">
                    <p class="pt-15 typo-black">
                        Participating teachers focus on workshop interaction.
                    </p>
                </div>
                <div class="img03">
                    <img src="images/psgEx4-08.png" alt="" class="img--sml" width="">
                    <img src="images/psgEx4-09.png" alt="" class="img--big" width="">
                    <p class="pt-15 typo-black">
                        2020 Innovative Teaching and Evaluation Seminar & LTTC Competency Oriented Assessment Workshop. 
                    </p>
                </div>
                <h3 class="pagExin04-sectionTit">
                    <span>
                        Enhancing Teachers' Competency Oriented Test Writing Knowledge 
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    The “Competency Oriented English Test Question Competition" was held to encourage teachers to improve their knowledge of Competency Oriented assessment. In addition, the winning test questions served as practical examples for educators, effectively linking learning, teaching and assessment. 
                </p>
            </div>
        </div>
    </div>
    
    <div class="pagExin04-contentBk pagExin04-contentBk--bgYellow">
        <div class="max_width">
            <div class="pagExin04-sectionBk pagExin04-sectionBk--12">
                <h3 class="pagExin04-sectionTit">
                    <span>
                        Taiwan English Learner Corpus (LTTC-ELC)
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    The LTTC built a two-million-word Taiwanese English Learner Corpus, based on all test answers from the GEPT, while providing free corpus analysis. The LTTC-ELC corpus provides teachers with scientific and systematic tools to analyze the blind spots and difficulties that Taiwanese students face when writing in English. These tools act as a key reference for teaching enhancement. The LTTC-ELC corpus provides three different search functions: keyword, collocation, and continuous multi-word (Ngram) search. 
                    <br>
                    • Keyword search: This can be used to understand common mistakes in the use of specific vocabulary. For example, “information” is an uncountable noun, but many learners often use the plural “informations”.
                    <br>
                    • Collocation search: This can be used to understand the common combination of words with other words. For example, the usage of “contact” as a verb (“contact someone”) and noun (“keep in contact with someone”) is easily confused. 
                    <br>
                    • Continuous multi-character (Ngram) search: This can be used to understand difficulties with items such as idioms or phrases. For example, the correct usage of “In my opinion” should be to directly introduce the following argument, but it turns out the frequency of sentence structures such as “In my opinion, I think...” is quite high in the corpus.       
                </p>
                <div class="img01">
                    <img src="images/psgEx4-10.png" alt="" class="" width="100%">
                </div>
            </div>
            
        </div>
    </div>

    
    <!-- 首頁底元素 -->
    <div class="pagExin04-contentBk--bgYellow">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     