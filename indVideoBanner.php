<!-- 影片祝賀輪播區 -->
<div class="indVideoLink">
	<!-- 背景圖輪播 -->
	<div class="indVideoLink-bg">
		<div class="indVideoLink-bg--img"></div>
	</div>

	<!-- 影片輪播祝賀 -->
	<div class="indVideoLink-banner owl-carousel owl-theme">
		<a href="https://youtu.be/qEMZOfU0gPo" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>LTTC董事長／臺大校長</h6>
			<h6 class="typoEntit mb-5">Chairman of the LTTC / President of NTU, TWN</h6>
			<h4>管中閔</h4>
			<p>LTTC將秉持一貫的專業精神，為臺灣外語教育帶來更多的正向影響</p>
		</a>
		<a href="https://youtu.be/L-RjiaRFIGs" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>英國文化協會測評研發主任</h6>
			<h6 class="typoEntit mb-5">Head of Assessment Research & Development, BC, UK</h6>
			<h4>Barry O'Sullivan</h4>
			<p>The LTTC is now seen as a leading player in language testing at the highest level of the profession.</p>
		</a>
		<a href="https://youtu.be/abBNn9hqrug" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>越南外國語大學校長</h6>
			<h6 class="typoEntit mb-5">President of ULIS, VN</h6>
			<h4>Do Tuan Minh</h4>
			<p>On this special occasion, we wish the LTTC more success in the future.</p>
		</a>
		<a href="https://youtu.be/S4_bkfQKIq4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>韓國高麗大學教授</h6>
			<h6 class="typoEntit mb-5">Professor, Korea Univ., KR</h6>
			<h4>Inn Chull Choi</h4>
			<p>The LTTC has been well recognized as one of the most prestigious institutions for training and testing internationally.</p>
		</a>
		<a href="https://youtu.be/0cpu2qXPdG4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>澳洲墨爾本大學部門副主任</h6>
			<h6 class="typoEntit mb-5">Deputy Director, Language Testing Research Centre, Univ. of Melbourne, AU</h6>
			<h4>Jason Fan</h4>
			<p>The LTTC has made significant contributions to the field of language assessment.</p>
		</a>
		<a href="https://youtu.be/fxoeu1fR6ps" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>英國蘭卡斯特大學教授</h6>
			<h6 class="typoEntit mb-5">Professor, Lancaster Univ., UK</h6>
			<h4>Luke Harding</h4>
			<p>Seventy years is a remarkable achievement, and the LTTC is a remarkable organization.</p>
		</a>
		<a href="https://youtu.be/Z7PX0IJjPcM" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>美國加州大學榮譽教授</h6>
			<h6 class="typoEntit mb-5">Professor Emeritus, UCLA, US</h6>
			<h4>Lyle Bachman</h4>
			<p>I've seen the LTTC evolve from a national institution, into one of the leading English language assessment centers in the world.</p>
		</a>
		<a href="https://youtu.be/ZIQXoVVM72M" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>英國劍橋考試院部門總監</h6>
			<h6 class="typoEntit mb-5">Director of Research and Thought Leadership, Cambridge English, UK</h6>
			<h4>Nick Saville</h4>
			<p>Congratulations to your 70th anniversary- that's a really impressive achievement!</p>
		</a>
		<a href="https://youtu.be/u0jczqhHVl8" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>英國文化協會臺灣處長</h6>
			<h6 class="typoEntit mb-5">Director of BC Taiwan</h6>
			<h4>Ralph Rogers</h4>
			<p>As Taiwan pursues its ambition to become bilingual in Chinese and English, the LTTC's work is only going to become increasingly important.</p>
		</a>
		<a href="https://youtu.be/NRsNdxIuJ2c" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>英國南安普敦大學資深講師</h6>
			<h6 class="typoEntit mb-5">Senior Teaching Fellow, Univ. of Southampton, UK</h6>
			<h4>Robert Baird</h4>
			<p>We've got great memories of collaborating with the wonderful people you have working there.</p>
		</a>
		<a href="https://youtu.be/2dIVLB5MSK4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>英國貝德福大學教授</h6>
			<h6 class="typoEntit mb-5">Director of CRELLA, Univ. of Bedfordshire, UK</h6>
			<h4>Tony Green</h4>
			<p>Thanks LTTC for supporting the international research community,  demonstrating real commitment to English language testing.</p>
		</a>
		<a href="https://youtu.be/Nl7X5xi1ZG4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>韓國首爾大學教授</h6>
			<h6 class="typoEntit mb-5">Professor, SNU, KR</h6>
			<h4>Yong-Won Lee</h4>
			<p>I appreciate the insight that LTTC has provided to language teaching and testing communities in Asia and beyond.</p>
		</a>
		<a href="https://youtu.be/0d0TD2304Bs" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>日本台灣交流協會代表</h6>
			<h6 class="typoEntit mb-5">Japan-Taiwan Exchange Association</h6>
			<h4></h4>
			<p>これからも日台の架け橋として、一緒に頑張って行きましょう。</p>
		</a>
		<a href="https://youtu.be/iGF_onn853Y" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>日本英語檢定協會EIKEN</h6>
			<h6 class="typoEntit mb-5">EIKEN, JPN</h6>
			<h4>仲村圭太</h4>
			<p>This is a monumental achievement, made possible by every staff member of the LTTC.</p>
		</a>
		<a href="https://youtu.be/CIZmvBvZ4R0" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>臺大日文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>朱秋而</h4>
			<p>LTTC每年舉辦兩次日本語能力試驗，對日語人才的培育貢獻很大</p>
		</a>
		<a href="https://youtu.be/pNx_aZnt6Ys" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>學術交流基金會董事</h6>
			<h6 class="typoEntit mb-5">Director, Fulbright Taiwan</h6>
			<h4>吳靜吉</h4>
			<p>讓國人的英文能力站上國際，讓人知道你是臺灣LTTC訓練出來的</p>
		</a>
		<a href="https://youtu.be/gPpkGarAGzM" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>教育部國際及兩岸教育司司長</h6>
			<h6 class="typoEntit mb-5">Director General, Dept. of Int'l & Cross-strait Education of the MoE, TWN</h6>
			<h4>李彥儀</h4>
			<p>很佩服LTTC的貢獻，並提供國人一個學習與評量的地方</p>
		</a>
		<a href="https://youtu.be/E3kAJ_kHJkM" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>臺科大人文社科學院院長</h6>
			<h6 class="typoEntit mb-5">Dean of LASS, NTUST, TWN</h6>
			<h4>李思穎</h4>
			<p>感恩LTTC持續無私的為臺灣的教育與學術研究奉獻</p>
		</a>
		<a href="https://youtu.be/RINMc-Wo2rc" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>臺大寫作教學中心主任</h6>
			<h6 class="typoEntit mb-5">AWEC of NTU, TWN</h6>
			<h4>李維晏與同仁</h4>
			<p>70年成就來自LTTC堅持初衷，長期投入臺灣的語言測驗和教育</p>
		</a>
		<a href="https://youtu.be/bjn3o_-qUY4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>臺大外文系名譽教授</h6>
			<h6 class="typoEntit mb-5">Professor Emeritus, NTU, TWN</h6>
			<h4>林耀福</h4>
			<p>LTTC是華人世界歷史最悠久，成就和貢獻最高的外語機構</p>
		</a>
		<a href="https://youtu.be/pD4ZO8ScYe8" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>日本京都大學iUP專案代表</h6>
			<h6 class="typoEntit mb-5">iUP Program, Kyoto Univ., JPN</h6>
			<h4>河合淳子</h4>
			<p>ご尽力に感謝申し上げております。今後のますますのご発展をお祈り申し上げます。</p>
		</a>
		<a href="https://youtu.be/VRAGpZ_TfaE" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>臺大外文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>邱錦榮</h4>
			<p>祝福 LTTC 帶領更多的學習者看見未來、掌握機會</p>
		</a>
		<a href="https://youtu.be/2d0fwKm0Lr4" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>上海交通大學教授</h6>
			<h6 class="typoEntit mb-5">Professor, SJTU, CN</h6>
			<h4>金艷</h4>
			<p>LTTC為臺灣學習者提供發展機會，也在國際學術界獲得高度肯定</p>
		</a>
		<a href="https://youtu.be/SKWVhMY-Wxg" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>臺大國際長</h6>
			<h6 class="typoEntit mb-5">VP of OIA, NTU, TWN</h6>
			<h4>袁孝維</h4>
			<p>人生70才開始，LTTC70歲生日快樂，大展鴻圖！</p>
		</a>
		<a href="https://youtu.be/PnsYnOcvzAs" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>世新大學外文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, SHU, TWN</h6>
			<h4>高天恩</h4>
			<p>LTTC不只在臺灣，在全球的語言教學、測驗方面，都是一個奇蹟</p>
		</a>
		<a href="https://youtu.be/j8wCU-QSt-Y" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>外交部外交及國際事務學院副院長</h6>
			<h6 class="typoEntit mb-5">President of IDIA, MOFA, TWN</h6>
			<h4>高安</h4>
			<p>LTTC 70年來走過的歲月，是專業與口碑的累積</p>
		</a>
		<a href="https://youtu.be/o2ztfaH-j8s" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>十二年國教英語科召集人</h6>
			<h6 class="typoEntit mb-5">Chairman of English, 12-yr Compulsory Education</h6>
			<h4>張武昌</h4>
			<p>LTTC成功從國際測驗代工蛻變為自主研發、接軌國際的品牌</p>
		</a>
		<a href="https://youtu.be/fRkA2LZXP3A" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>臺大外文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>張淑英</h4>
			<p>祝LTTC繼續領導臺灣的外語測驗教學與技術輸出，成為國際標竿</p>
		</a>
		<a href="https://youtu.be/sKewpOsOk2U" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>國發會社會發展處處長</h6>
			<h6 class="typoEntit mb-5">Director of DSD, NDC, TWN</h6>
			<h4>張富林</h4>
			<p>LTTC開拓國際合作、深耕語言教學與研究，也培養許多外語菁英</p>
		</a>
		<a href="https://youtu.be/9ouzpgMUrtw" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>國發會綜合規劃處處長</h6>
			<h6 class="typoEntit mb-5">Director of DOP, NDC, TWN</h6>
			<h4>張惠娟</h4>
			<p>LTTC在臺灣外語學習領域所扮演的角色是無可取代的</p>
		</a>
		<a href="https://youtu.be/gU4hHhnsCZ0" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>臺大外文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>梁欣榮</h4>
			<p>LTTC在臺建立起完整的英語檢測和培訓機制，為華人世界的創舉</p>
		</a>
		<a href="https://youtu.be/o2WM9c6bDGE" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>文藻外語大學校長</h6>
			<h6 class="typoEntit mb-5">President of WZU, TWN</h6>
			<h4>陳美華</h4>
			<p>感謝LTTC在外語教育的貢獻，讓我們看見臺灣在全球競爭的潛力</p>
		</a>
		<a href="https://youtu.be/KNbEyZHBP_Y" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>臺大師培中心主任</h6>
			<h6 class="typoEntit mb-5">Director, Center for Teacher Education, NTU, TWN</h6>
			<h4>傅昭銘</h4>
			<p>LTTC有很專業的師資團隊，可對臺灣雙語教育做出貢獻</p>
		</a>
		<a href="https://youtu.be/tYYMF02vEjE" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>南臺科大人文社會學院院長</h6>
			<h6 class="typoEntit mb-5">Dean, College of Humanities and Social Sciences, STUST, TWN</h6>
			<h4>黃大夫</h4>
			<p>感謝LTTC為本校提供外語教學及檢測，LTTC是值得長期合作及信賴的專業機構。</p>
		</a>
		<a href="https://youtu.be/Z2hVNnT4thY" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>幫你優(Bonio)執行長</h6>
			<h6 class="typoEntit mb-5">CEO, BoniO Inc.</h6>
			<h4>葉丙成</h4>
			<p>LTTC 70歲還有這麼多活力與能量持續為臺灣貢獻，很不簡單</p>
		</a>
		<a href="https://youtu.be/7vt09oLW8CY" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style01">
			<h6>臺大外文系教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>廖咸浩</h4>
			<p>期待LTTC持續領航臺灣的語言訓練界甚至亞洲與更大的領域</p>
		</a>
		<a href="https://youtu.be/p1Kh-CaEYuA" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style02">
			<h6>臺大語言所所長</h6>
			<h6 class="typoEntit mb-5">Chair of GIL, NTU, TWN</h6>
			<h4>謝舒凱</h4>
			<p>感謝LTTC支持中學生語言學奧林匹亞競賽，讓團隊取得豐碩成果</p>
		</a>
		<a href="https://youtu.be/mTxBQiReQus" target="_blank" class="indVideoLink-banner--link indVideoLink-banner--link--style03">
			<h6>臺大語言所教授</h6>
			<h6 class="typoEntit mb-5">Professor, NTU, TWN</h6>
			<h4>蘇以文</h4>
			<p>祝賀LTTC繼續努力，為臺灣的語言教育、訓練與測驗做更多的貢獻</p>
		</a>
	</div>       
</div>