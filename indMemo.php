<!-- 特效 -->
<script language="javascript">
    $(document).ready(function () {
        $("#js-1951-1970").on('click', function () {
            $(".indMemo-yearHrefBk--listBk--list").removeClass("indMemo-yearHrefBk--listBk--listIn");
            $(".indMemo").scrollLeft(305);
            $("#js-1951-1970").addClass("indMemo-yearHrefBk--listBk--listIn");
        });
        $("#js-1971-1990").on('click', function () {
            $(".indMemo-yearHrefBk--listBk--list").removeClass("indMemo-yearHrefBk--listBk--listIn");
            $(".indMemo").scrollLeft(2700);
            $("#js-1971-1990").addClass("indMemo-yearHrefBk--listBk--listIn");
        });
        $("#js-1991-2000").on('click', function () {
            $(".indMemo-yearHrefBk--listBk--list").removeClass("indMemo-yearHrefBk--listBk--listIn");
            $(".indMemo").scrollLeft(5580);
            $("#js-1991-2000").addClass("indMemo-yearHrefBk--listBk--listIn");
        });
        $("#js-2001-2010").on('click', function () {
            $(".indMemo-yearHrefBk--listBk--list").removeClass("indMemo-yearHrefBk--listBk--listIn");
            $(".indMemo").scrollLeft(8465);
            $("#js-2001-2010").addClass("indMemo-yearHrefBk--listBk--listIn");
        });
        $("#js-2011-2021").on('click', function () {
            $(".indMemo-yearHrefBk--listBk--list").removeClass("indMemo-yearHrefBk--listBk--listIn");
            $(".indMemo").scrollLeft(13260);
            $("#js-2011-2021").addClass("indMemo-yearHrefBk--listBk--listIn");
        });
    });
</script>


<!-- 大事記區 -->
<div class="indMemoBk">
    <div id="milestones-ahref" class="indMemoBk--ahref"></div>
    <!-- 遮罩動畫 -->
    <div class="indMemoBk--layer">
        <img src="images/indele08.svg" alt="hand" class="indMemoBk--hand">
        <p class="indMemoBk--word">
        Please select a year from the buttons above or drag your mouse <br />  (finger if using a touchscreen) <br /> 
        left or right to view different years. Click to close this message.
        </p>
    </div>
    <!-- tit -->
    <div class="indMemo-titBk">
        <div class="indMemo-tit">
            <h6 class="indMemo-titBk--en">Milestones</h6>
        </div>
    </div>

    <!-- 選擇年分 -->
    <div class="indMemo-yearHrefBk">
        <h6 class="indMemo-yearHrefBk--tit typo-black">Browse by years：</h6>
        <div class="indMemo-yearHrefBk--listBk">
            <a href="javascript:void(0);" id="js-1951-1970" class="indMemo-yearHrefBk--listBk--list">1951-1970</a>
            <a href="javascript:void(0);" id="js-1971-1990" class="indMemo-yearHrefBk--listBk--list">1971-1990</a>
            <a href="javascript:void(0);" id="js-1991-2000" class="indMemo-yearHrefBk--listBk--list">1991-2000</a>
            <a href="javascript:void(0);" id="js-2001-2010" class="indMemo-yearHrefBk--listBk--list">2001-2010</a>
            <a href="javascript:void(0);" id="js-2011-2021" class="indMemo-yearHrefBk--listBk--list">2011-2021</a>
        </div>
    </div>

    <!-- 元素 -->
    <div class="indMemo-eleBk">
        <img src="images/indele05.png" alt="" class="indMemo-eleBk--01">
        <img src="images/indele06.png" alt="" class="indMemo-eleBk--02">
        <img src="images/indele07.png" alt="" class="indMemo-eleBk--03">
    </div>

    <!-- 大事記區 -->
    <div class="indMemo">
        <div class="indMemo-barTitBk">
            <p class="indMemo-barTit mt-15">Milestones for Foreign Language <br />Education in Taiwan</p>
            <p class="indMemo-barTit mt-50">Milestones in LTTC History</p>
        </div>
        <img src="images/indele12.svg" alt="bar" class="indMemo-bar">

        <!-- 01 -->
        <p id="back01" class="indMemo-year indMemo-year01">1951</p>
        <div class="indMemo-tai indMemo-tai01">
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The US begins an extensive aid program in Taiwan to provide military and economic assistance,
                        including funding for culture and education.
                    </span>
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc01">
            <article class="indMemo-content">
                <img src="images/indimg01.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Under the US aid program, the Language Training & Testing Center (LTTC) is founded under the
                        name “English Training Center.”
                    </span>
                </p>
            </article>
        </div>

        <!-- 02 -->
        <p id="" class="indMemo-year indMemo-year02">1965</p>
        <div class="indMemo-lttc indMemo-lttc02">
            <article class="indMemo-content">
                <img src="images/indimg02.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The English Training Center is renamed the Language Center, coinciding with the establishment of
                        Taiwan’s first language lab. The Foreign Language Proficiency Test (FLPT) is launched the same
                        year under the administration of the Language Center.
                    </span>
                    <a href="#1965" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 03 -->
        <p id="back1966" class="indMemo-year indMemo-year03">1966</p>
        <div class="indMemo-lttc indMemo-lttc03">
            <article class="indMemo-content">
                <img src="images/indimg03.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Language Center starts to administer the Test of English as a Foreign Language (TOEFL) on
                        behalf of the US Educational Testing Service (ETS). </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 04 -->
        <p id="back1968" class="indMemo-year indMemo-year04">1968</p>
        <div class="indMemo-tai indMemo-tai04">
            <article class="indMemo-content">
                <!-- <img src="images/indimg03.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Taiwan extends compulsory education to nine years , marking a new chapter in the nation’s
                        development.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 05 -->
        <p id="back1970" class="indMemo-year indMemo-year05">1970</p>
        <div class="indMemo-lttc indMemo-lttc05">
            <article class="indMemo-content">
                <img src="images/indimg04.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Tourism Bureau adopts the FLPT as a requirement for tour guide certification.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 06 -->
        <p id="back1976" class="indMemo-year indMemo-year06">1976</p>
        <div class="indMemo-lttc indMemo-lttc06">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Taiwan’s Ministry of Education (MOE) adopts the FLPT for assessing the language abilities of
                        applicants wishing to pursue academic studies in Japan and Europe.</span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 07 -->
        <p id="back1979" class="indMemo-year indMemo-year07">1979</p>
        <div class="indMemo-tai indMemo-tai07">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The US government breaks diplomatic ties with Taiwan.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc07">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Language Center is officially renamed “The Language Training & Testing Center,” or the LTTC.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 08 -->
        <p id="back1983" class="indMemo-year indMemo-year08">1983</p>
        <div class="indMemo-tai indMemo-tai08">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE mandates the inclusion of 2<sup>nd</sup> foreign language elective courses for senior high school
                        students.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc08">
            <article class="indMemo-content">
                <!-- <img src="images/indimg04.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC begins to administer the Test of English for International Communication (TOEIC) on
                        behalf of the ETS.
                    </span>
                    <!-- <a href="#1965" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 09 -->
        <p id="back1984" class="indMemo-year indMemo-year09">1984</p>
        <div class="indMemo-lttc indMemo-lttc09">
            <article class="indMemo-content">
                <img src="images/indimg05.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the MOE to organize collegiate-level speech contests in English and
                        Japanese.
                    </span>
                    <a href="#1984" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 010 -->
        <p id="back1985" class="indMemo-year indMemo-year010">1985</p>
        <div class="indMemo-lttc indMemo-lttc010">
            <article class="indMemo-content">
                <img src="images/indimg06.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC moves into National Taiwan University’s Language Center Building, where it continues to
                        operate today.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 011 -->
        <p id="back1986" class="indMemo-year indMemo-year011">1986</p>
        <div class="indMemo-lttc indMemo-lttc011">
            <article class="indMemo-content">
                <img src="images/indimg07.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC officially registers to become an educational foundation.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 012 -->
        <p id="back1991" class="indMemo-year indMemo-year012">1991</p>
        <div class="indMemo-lttc indMemo-lttc012">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Japanese-Language Proficiency Test (JLPT).
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 013 -->
        <p id="back1996" class="indMemo-year indMemo-year013">1996</p>
        <div class="indMemo-tai indMemo-tai013">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE selects three pilot schools to provide 2nd foreign language instruction at senior high
                        schools.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc013">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC establishes the Prometric Testing Center to administer computer-based tests.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 014 -->
        <p id="back1997" class="indMemo-year indMemo-year014">1997</p>
        <div class="indMemo-lttc indMemo-lttc014">
            <article class="indMemo-content">
                <img src="images/indimg07-1.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC launches the College Student English Proficiency Test (CSEPT).
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 015 -->
        <p id="back1998" class="indMemo-year indMemo-year015">1998</p>
        <div class="indMemo-tai indMemo-tai015">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE officially introduces a new policy to promote the concept of “life-long learning,” which
                        includes the promotion of foreign language learning.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc015">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Graduate Record Examinations (GRE) General Test.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 016 -->
        <p id="back1998" class="indMemo-year indMemo-year016">1999</p>
        <div class="indMemo-tai indMemo-tai016">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE launches a five-year initiative to establish 2nd foreign language courses at senior high
                        schools across the entire nation.
                    </span>
                    <!-- <a href="#1985" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc016">
            <article class="indMemo-content">
                <!-- <img src="images/indimg07-1.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        To further promote “life-long learning,” the MOE provides a funding grant to the LTTC to begin
                        development of the General English Proficiency Test (GEPT).
                    </span>
                    <a href="#1999" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 017 -->
        <p id="back2000" class="indMemo-year indMemo-year017">2000</p>
        <div class="indMemo-lttc indMemo-lttc017">
            <article class="indMemo-content">
                <img src="images/indimg08.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Intermediate-level GEPT is launched.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg08.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC publishes a 4-level Japanese coursebook entitled <i>日本語 GoGoGo</i>.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 018 -->
        <p id="back2001" class="indMemo-year indMemo-year018">2001</p>
        <div class="indMemo-tai indMemo-tai018">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE introduces major revisions to its nine-year compulsory education curriculum.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Compulsory English education is implemented in elementary schools, starting in 5th and 6th
                        grades.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc018">
            <article class="indMemo-content">
                <img src="images/indimg09.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Prof. Cyril Weir of the University of Reading joins the GEPT research committee.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the MOE to work with technical and vocational schools to develop
                        English assessment tests for students and training programs for English teachers.
                    </span>
                    <a href="#2001" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 019 -->
        <p id="back2002" class="indMemo-year indMemo-year019">2002</p>
        <div class="indMemo-tai indMemo-tai019">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Taiwan joins the World Trade Organization (WTO).
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Executive Yuan announces the <i>Challenge 2008: National Development Plan</i>, which
                        includes plans to create “an English speaking environment” across the nation.
                    </span>
                    <a href="#2002" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc019">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Personnel Administration Office of the Executive Yuan adopts the FLPT as screening criteria
                        for civil servants seeking to conduct research overseas or attend short-term English training
                        courses.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Examination for Japanese University Admission for
                        International Students (EJU).
                    </span>
                    <!-- <a href="#2002" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Test Deutsch als Fremdsprache (TestDaF).
                    </span>
                    <!-- <a href="#2002" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 020 -->
        <p id="back2003" class="indMemo-year indMemo-year020">2003</p>
        <div class="indMemo-lttc indMemo-lttc020">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC establishes a scholarship program for postgraduate students in linguistics.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 021 -->
        <p id="back2004" class="indMemo-year indMemo-year021">2004</p>
        <div class="indMemo-lttc indMemo-lttc021">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The total number of GEPT test takers exceeds one million.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The FLPT and GEPT are officially adopted by the Executive Yuan as measures of language
                        proficiency for civil servants seeking promotion.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer Cambridge Assessment English exams.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 022 -->
        <p id="back2005" class="indMemo-year indMemo-year022">2005</p>
        <div class="indMemo-tai indMemo-tai022">
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        English language education is officially expanded to begin at 3rd grade at elementary schools
                        across the nation.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE issues official guidelines on the use of English examinations for certification
                        purposes.
                    </span>
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc022">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <img src="images/pagEx02-38.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The first comparison study of the GEPT and the Common European Framework of Reference is
                        conducted.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 023 -->
        <p id="back2006" class="indMemo-year indMemo-year023">2006</p>
        <div class="indMemo-lttc indMemo-lttc023">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer Japan’s Licensed Guide Interpreter Test in Taiwan.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 024 -->
        <p id="back2007" class="indMemo-year indMemo-year024">2007</p>
        <div class="indMemo-lttc indMemo-lttc024">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The GEPT is administered in Vietnam, marking the first time the test is administered overseas.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with the Graduate Institute of Linguistics at National Taiwan University
                        to create the LTTC English Learner Corpus and related online learning tools.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC begins holding English academic writing courses at Academia Sinica, the nation’s top
                        research institution.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 025 -->
        <p id="back2008" class="indMemo-year indMemo-year025">2008</p>
        <div class="indMemo-tai indMemo-tai025">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Executive Yuan continues with official plans “to create an environment friendly to the
                        international community.”
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Foreign Language Education Center for Senior High Schools is established.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc025">
            <article class="indMemo-content">
                <img src="images/indimg11.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Prof. Charles Alderson from Lancaster Univ., Prof. Lyle Bachman and Prof. Antony Kunnan from
                        UCLA, and Prof. Tim McNamara from the Univ. of Melbourne join the LTTC as consultants for
                        language test development.
                    </span>
                    <a href="#2008" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 026 -->
        <p id="back2009" class="indMemo-year indMemo-year026">2009</p>
        <div class="indMemo-tai indMemo-tai026">
            <article class="indMemo-content">
                <!-- <img src="images/indimg10.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Executive Yuan begins a new major public infrastructure initiative, which includes programs
                        to improve the English proficiency of all citizens.
                    </span>
                    <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc026">
            <article class="indMemo-content">
                <img src="images/indimg12.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC hosts its first major international academic conference. The LTTC now regularly holds
                        conferences in the fields of language teaching and testing.
                    </span>
                    <!-- <a href="#2008" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg12.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC begins an award program for outstanding GEPT Essays.
                    </span>
                    <a href="#2009" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 027 -->
        <p id="back2010" class="indMemo-year indMemo-year027">2010</p>
        <div class="indMemo-lttc indMemo-lttc027">
            <article class="indMemo-content">
                <img src="images/indimg13.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The GEPT is internationally recognized: 97 tertiary institutes across 27 countries adopt it as a
                        reliable means of measuring Taiwanese students’ English language ability.
                    </span>
                    <a href="#2010" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 028 -->
        <p id="back2011" class="indMemo-year indMemo-year028">2011</p>
        <div class="indMemo-lttc indMemo-lttc028">
            <article class="indMemo-content">
                <img src="images/indimg14.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC launches the Second Foreign Language Proficiency Test (SFLPT).
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 029 -->
        <p id="back2012" class="indMemo-year indMemo-year029">2012</p>
        <div class="indMemo-lttc indMemo-lttc029">
            <article class="indMemo-content">
                <img src="images/indimg15.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC launches the first GEPT app.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC obtains certification under the Bureau of Employment and Vocational Training’s Taiwan
                        TrainQuali System.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 030 -->
        <p id="back2013" class="indMemo-year indMemo-year030">2013</p>
        <div class="indMemo-lttc indMemo-lttc030">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC’s Administrative Operating System gains ISO9001 quality management certification.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC publishes the first issue of the journal <i>The Way of Language</i>.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 031 -->
        <p id="back2014" class="indMemo-year indMemo-year031">2014</p>
        <div class="indMemo-tai indMemo-tai031">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE promulgates the general curricular framework for Twelve-Year Compulsory Education.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc031">
            <article class="indMemo-content">
                <img src="images/indimg16.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC provides workshops on the development of English assessment for the University of
                        Language and International Studies (ULIS), Vietnam.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with elementary schools in Taipei on piloting courses in Japanese and
                        European languages for kids.
                    </span>
                    <a href="#2014" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 032 -->
        <p id="back2015" class="indMemo-year indMemo-year032">2015</p>
        <div class="indMemo-tai indMemo-tai032">
            <article class="indMemo-content">
                <!-- <img src="images/indimg15.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        English listening assessment is included in both college entrance exams and the Comprehensive
                        Assessment Program for Junior High School Students.
                    </span>
                    <!-- <a href="#2010" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc032">
            <article class="indMemo-content">
                <img src="images/indimg17.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        GEPT Kids is launched, featuring 18 items of individualized diagnostic feedback.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC begins to fund English teaching activities for elementary and middle schools in remote
                        areas.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 033 -->
        <p id="back2016" class="indMemo-year indMemo-year033">2016</p>
        <div class="indMemo-lttc indMemo-lttc033">
            <article class="indMemo-content">
                <img src="images/indimg18.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC collaborates with Feng Chia University on providing FLPT- and SFLPT-related resources
                        and establishing the evaluation standards for foreign language courses.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned to administer the Test of Proficiency in Korean (TOPIK).
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 034 -->
        <p id="back2017" class="indMemo-year indMemo-year034">2017</p>
        <div class="indMemo-tai indMemo-tai034">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Executive Yuan re-proposes the idea of stipulating English as an official language in
                        Taiwan.
                    </span>
                    <a href="#2017-1" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc034">
            <article class="indMemo-content">
                <img src="images/indimg18-1.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC hosts conferences for the Asian Association for Language Assessment and the Academic
                        Forum on English Language Testing in Asia.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <!-- <img src="images/indimg17.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC accepts a commission from the MOE in Vietnam to conduct workshops on the logistics of
                        administering large-scale English tests—using the GEPT as an example.
                    </span>
                    <!-- <a href="#2017-2" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 035 -->
        <p id="back2018" class="indMemo-year indMemo-year035">2018</p>
        <div class="indMemo-tai indMemo-tai035">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The National Development Council promulgates the policy of transforming Taiwan into a bilingual
                        country by 2030.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc035">
            <article class="indMemo-content">
                <!-- <img src="images/indimg20.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC establishes the LTTC Teaching and Research Grant program.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>

        <!-- 036 -->
        <p id="back2019" class="indMemo-year indMemo-year036">2019</p>
        <div class="indMemo-tai indMemo-tai036">
            <article class="indMemo-content">
                <!-- <img src="images/indimg18.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Taiwanese government officially implements the 2019 competency-driven curriculum.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc036">
            <article class="indMemo-content">
                <img src="images/indimg21.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        A special volume entitled <i>English Language Proficiency Testing: A New Paradigm Bridging
                            Global and Local Contexts</i> is published.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC is commissioned by the Taipei City Government Department of Education to develop
                        listening and speaking assessment tools adopting a CLIL approach for 1st- and 2nd-grade life
                        science courses.
                    </span>
                    <a href="#2019" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 037 -->
        <p id="back2020" class="indMemo-year indMemo-year037">2020</p>
        <div class="indMemo-tai indMemo-tai037">
            <article class="indMemo-content">
                <!-- <img src="images/indimg22.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The MOE announces its “White Paper on International Education for Primary and Secondary Schools
                        2.0” with 13 actions plans including the promotion of bilingual education.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc037">
            <article class="indMemo-content">
                <!-- <img src="images/indimg22.png" alt="" class="indMemo-content--img"> -->
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        Thanks to Taiwan’s successful prevention and control of COVID-19, the LTTC becomes the only
                        institute to administer TOPIK during this period.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC celebrates the GEPT’s 20th Anniversary. The number of test takers exceeds eight
                        million.
                    </span>
                    <a href="#2020" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

        <!-- 038 -->
        <p id="back2021" class="indMemo-year indMemo-year038">2021</p>
        <div class="indMemo-tai indMemo-tai038">
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The Taipei City Government selects one senior high school to pilot the International
                        Baccalaureate (IB) program.
                    </span>
                    <!-- <a href="#2017-1" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
        </div>
        <div class="indMemo-lttc indMemo-lttc038">
            <article class="indMemo-content">
                <img src="images/indimg23.png" alt="" class="indMemo-content--img">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        In response to the competency-driven curriculum, the LTTC adjusts the assessment design of the
                        GEPT’s listening and reading components. Meanwhile, The LTTC also launches the GEPT Diagnosis
                        Room initiative to offer individualized feedback to test takers.
                    </span>
                    <!-- <a href="#2014" class="indMemo-content--moreBt">more...</a> -->
                </p>
            </article>
            <article class="indMemo-content">
                <span class="indMemo-content--label">➤</span>
                <p class="indMemo-content--text">
                    <span class="indMemo-content--enText">
                        The LTTC celebrates its 70th anniversary and its 35th anniversary as a registered educational
                        foundation.
                    </span>
                    <a href="#2021" class="indMemo-content--moreBt">more...</a>
                </p>
            </article>
        </div>

    </div>


    <!-- light box -->
    <!-- 1965 -->
    <div class="modLightbox" id="1965">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1965 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The English Training Center is renamed the Language Center, coinciding with the
                                establishment of Taiwan’s first language lab. The Foreign Language Proficiency Test
                                (FLPT) is launched the same year under the administration of the Language Center.
                            </span>
                        </p>
                    </article>
                    <img src="images/indimg24.png" alt="" class="indMemo-content--img">
                </article>
            </div>
        </div>
    </div>

    <!-- 1984 -->
    <div class="modLightbox" id="1984">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1984 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the MOE to organize collegiate-level speech contests in
                                English and Japanese.
                            </span>
                        </p>
                    </article>
                    <img src="images/indimg25.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC launches English, Japanese, and French language courses tailored for pilots and
                                flight attendants at China Airlines.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 1999 -->
    <div class="modLightbox" id="1999">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">1999 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                To further promote “life-long learning,” the MOE provides a funding grant to the LTTC to
                                begin development of the General English Proficiency Test (GEPT).
                            </span>
                        </p>
                    </article>
                    <img src="images/indimg26.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MOE to develop and administer a special test for
                                assessing the language proficiency of English teachers at elementary schools.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2001 -->
    <div class="modLightbox" id="2001">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2001 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                Prof. Cyril Weir of the University of Reading joins the GEPT research committee.
                            </span>
                        </p>
                    </article>
                    <img src="images/indimg28.png" alt="" class="indMemo-content--img">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the MOE to work with technical and vocational schools to
                                develop English assessment tests for students and training programs for English
                                teachers.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC begins an award program for outstanding thesis and dissertation work in
                                language assessment and teaching.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2002 -->
    <div class="modLightbox" id="2002">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2002 Taiwan Foreign Language Education Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                Taiwan joins the World Trade Organization (WTO).
                            </span>
                            <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The Executive Yuan announces the <i>Challenge 2008: National Development Plan</i>, which
                                includes plans to create “an English speaking environment” across the nation.
                            </span>
                            <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The Executive Yuan proposes to make English one of Taiwan’s official languages.
                            </span>
                            <!-- <a href="#2000" class="indMemo-content--moreBt">more...</a> -->
                        </p>
                    </article>
            </div>
        </div>
    </div>

    <!-- 2005 -->
    <div class="modLightbox" id="2005">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">
                    2005 LTTC Milestones
                </h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                English language education is officially expanded to begin at 3rd grade at elementary
                                schools across the nation.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The MoE issues official guidelines on the use of English examinations for certification
                                purposes.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <!-- <img src="images/indimg09.png" alt="" class="indMemo-content--img"> -->
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The MoE starts the second phase of its five-year initiative to offer 2nd foreign
                                language education at senior high schools across the nation.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2008 -->
    <div class="modLightbox" id="2008">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2008 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                Prof. Charles Alderson from Lancaster Univ., Prof. Lyle Bachman and Prof. Antony Kunnan
                                from UCLA, and Prof. Tim McNamara from the Univ. of Melbourne join the LTTC as
                                consultants for language test development.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <img src="images/indimg29.png" alt="" class="indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2009 -->
    <div class="modLightbox" id="2009">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2009 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC hosts its first major international academic conference. The LTTC now regularly
                                holds conferences in the fields of language assessment and testing.
                            </span>
                        </p>
                        <img src="images/indimg30.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC begins an award program for outstanding GEPT Essays.
                            </span>
                        </p>
                        <img src="images/indimg31.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2010 -->
    <div class="modLightbox" id="2010">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2010 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The GEPT is internationally recognized: 97 tertiary institutes across 27 countries adopt
                                it as a reliable means of measuring Taiwanese students’ English language ability.
                            </span>
                        </p>
                        <img src="images/indimg13.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC launches the GEPT online self-evaluation system.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC establishes the LTTC-GEPT Research Grant program.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC administers the GEPT in Dongguan and Huadong, China, at schools for students of
                                Taiwanese business people.
                            </span>
                        </p>
                        <img src="images/indimg33.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MOE to develop and administer Proficiency Tests
                                in Chinese/English Translation and Interpretation.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC celebrates the GEPT’s 10th Anniversary. The number of test takers exceeds four
                                million.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2014 -->
    <div class="modLightbox" id="2014">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2014 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC provides workshops on the development of English assessment for the University
                                of Language and International Studies (ULIS), Vietnam.
                            </span>
                        </p>
                        <img src="images/indimg34.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC collaborates with elementary schools in Taipei on piloting courses in Japanese
                                and European languages for kids.
                            </span>
                        </p>
                        <img src="images/indimg35.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The number of GEPT test takers exceeds six million.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2017-1 -->
    <div class="modLightbox" id="2017-1">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2017 Taiwan Foreign Language Education Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The Executive Yuan re-proposes the idea of stipulating English as an official language
                                in Taiwan.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The Taipei City Government selects two elementary schools to pilot experimental
                                bilingual courses adopting a CLIL approach.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The College Entrance Examination Center announces the introduction of new item types in
                                2022 in response to the 2019 competency-driven curriculum.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2017-2 -->
    <div class="modLightbox" id="2017-2">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2017 臺灣外語教育大事記</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC hosts conferences for the Asian Association for Language Assessment and the
                                Academic Forum on English Language Testing in Asia.
                            </span>
                        </p>
                        <img src="images/indimg36.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC accepts a commission from the MoE in Vietnam to conduct workshops on the
                                logistics of administering large-scale English tests—using the GEPT as an example.
                            </span>
                        </p>
                        <img src="images/indimg37.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC adopts an automated scoring system for the GEPT.
                            </span>
                        </p>
                        <img src="images/indimg38.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2019 -->
    <div class="modLightbox" id="2019">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2019 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <img src="images/indimg39.png" alt="" class="mb-12 indMemo-content--img">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                A special volume entitled <i>English Language Proficiency Testing: A New Paradigm
                                    Bridging Global and Local Contexts</i> is published.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Department of Education, Taipei City Government, 
                                to develop listening and speaking assessment tools adopting a CLIL approach for 1st- and
                                2nd-grade life science courses.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC offers professional development activities and courses for in-service teachers,
                                focusing on language teaching and assessment.
                            </span>
                        </p>
                        <img src="images/indimg40.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2020 -->
    <div class="modLightbox" id="2020">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2020 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                Thanks to Taiwan’s successful prevention and control of COVID-19, the LTTC becomes the
                                only institute to administer TOPIK during this period.
                            </span>
                        </p>
                        <img src="images/indimg41.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC celebrates the GEPT’s 20th Anniversary. The number of test takers exceeds eight
                                million.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                Smart English Competency Alliance (SECA) online courses are launched.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <img src="images/indimg42.png" alt="" class="mt-25 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by several government organizations and educational
                                institutions to provide workshops on Content and Language Integrated Learning (CLIL) for
                                more than 400 bilingual teachers.
                            </span>
                        </p>
                        <br />
                        <img src="images/indimg43.png" alt="" class="mt-25 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Ministry of Foreign Affairs to offer intensive
                                professional English courses for trainees who have passed the civil service examination
                                for diplomats and foreign postings.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Taipei City Government Department of Education to
                                organize foreign language contests for high school students.
                            </span>
                        </p>
                        <br />
                        <img src="images/indimg44.png" alt="" class="mt-25 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC assists the National Civil Service Academy to organize English language
                                workshops to help civil servants improve their professional communication and writing
                                skills.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC produces educational and entertaining videos to help learners understand the
                                services provided by the LTTC.
                            </span>
                        </p>
                        <br />
                        <img src="images/indimg45.png" alt="" class="mt-25 indMemo-content--img">
                    </article>
                </article>
            </div>
        </div>
    </div>

    <!-- 2021 -->
    <div class="modLightbox" id="2021">
        <div class="modLightbox-contentBk">
            <a href="javascript:history.back();" class="modLightbox-close"></a>
            <div class="modLightbox-content">
                <h5 class="indLightbox-content--tit">2021 LTTC Milestones</h5>
                <article class="indLightbox-content--text">
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                In response to the competency-driven curriculum, the LTTC adjusts the assessment design
                                of the GEPT’s listening and reading components. Meanwhile, The LTTC also launches the
                                GEPT Diagnosis Room initiative to offer individualized feedback to test takers.
                            </span>
                        </p>
                        <img src="images/indimg46.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC celebrates its 70th anniversary and its 35th anniversary as a registered
                                educational foundation.
                            </span>
                        </p>
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC publishes two special volumes, one on EMI in Chinese-speaking regions, the other on GEPT test development.
                            </span>
                        </p>
                        <img src="images/indimg47.png" alt="" class="mt-12 indMemo-content--img">
                        <br />
                        <br />
                        <img src="images/indimg54.png" alt="" class="mt-12 indMemo-content--img">
                    </article>
                    <article class="indMemo-content">
                        <span class="indMemo-content--label">➤</span>
                        <p class="indMemo-content--text">
                            <span class="indMemo-content--enText">
                                The LTTC is commissioned by the Ministry of Defense to offer English communication
                                courses for military attachés.
                            </span>
                        </p>
                    </article>
                </article>
            </div>
        </div>
    </div>

</div>