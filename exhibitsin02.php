<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>


<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script>
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="">
    
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin02-bannerBk">
        <div class="pagExin02-pageTitBk">
            <h1 class="">
                From Taiwan to the World
            </h1>
        </div>
        <img src="images/pagEx02-01.png" alt="" class="pagExin02-banner">
    </div>
    
    <div class="pagExin02-section01Bk">
        <div class="max_width">
            <div class="pagExin02-section01">
                <div class="pagExin02-section01--textArea">
                    <h3 class="pagExin02-section01--textArea--tit"><span>Recognized Internationally</span></h3>
                    <p class="pt-15 typo-black">
                        The GEPT is accepted by 97 institutions in 27 countries as a testing standard for student English proficiency. 
                    </p>
                </div>
                <img src="images/pagEx02-02.png" alt="" class="pagExin02-section01--img">
            </div>
        </div>
    </div>

    <div class="pagExin02-section02Bk ptb-20">
        <div class="max_width">
            <img src="images/pagEx02-03.png" alt="" class="pagExin02-section02--img" width="100%">
            
            <div class="pagExin02-section02Bk--textArea">
                <div class="owl-custom01 owl-carousel owl-theme">
                    <section class="pagExin02-section02Bk--textArea--line">
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-04.png" alt="" class="mr-25">
                            <p class="">
                                Canada<br />
                                Université du Québec à Montréal<br />
                                University of Victoria
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-05.png" alt="" class="mr-25">
                            <p class="">
                                U.S.A.<br />
                                North Carolina State University<br />
                                University of California, Berkeley<br />
                                University of Illinois at Urbana-Champaign<br />
                                University of Oregon<br />
                                Arizona State University<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-06.png" alt="" class="mr-25">
                            <p class="">
                                U.K<br />
                                Hertford College, University of Oxford<br />
                                King's College London<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-07.png" alt="" class="mr-25">
                            <p class="">
                                Germany<br />
                                Technische Universität Berlin<br />
                                Universität Hamburg<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-08.png" alt="" class="mr-25">
                            <p class="">
                                Netherlands<br />
                                Maastricht University<br />
                            </p>
                        </article>
                    </section>
                    <section class="pagExin02-section02Bk--textArea--line">
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-09.png" alt="" class="mr-25">
                            <p class="">
                                France<br />
                                Université Catholique de Lille<br />
                                CentraleSupélec<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-10.png" alt="" class="mr-25">
                            <p class="">
                                Singapore<br />
                                Nanyang Technological University<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-11.png" alt="" class="mr-25">
                            <p class="">
                                Korea<br />
                                Kookmin University<br />
                                Seoul National University<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-12.png" alt="" class="mr-25">
                            <p class="">
                                Japan<br />
                                Waseda University<br />
                                Yokohama National University<br />
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-13.png" alt="" class="mr-25">
                            <p class="">
                                Hong Kong<br />
                                The Hong Kong Polytechnic University
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-14.png" alt="" class="mr-25">
                            <p class="">
                                Macau<br />
                                University of Macau
                            </p>
                        </article>
                        <article class="pagExin02-section02Bk--textArea--section">
                            <img src="images/pagEx02-15.png" alt="" class="mr-25">
                            <p class="">
                                New Zealand<br />
                                The University of Auckland
                            </p>
                        </article>
                    </section>
                </div>
            </div>
        </div>
    </div>
    
    <div class="pagExin02-sectionWhiteBk overflow-hidden">
        <div class="max_width">
            <article class="pagExin02-section03">
                <h3 class="typo-textAlignCenter mb-10">Connected to the World</h3>
                <p class="pt-15 typo-black">
                    The LTTC is active in the international sphere when it comes to language teaching and testing. We work with academic institutions across the world to develop language courses that prepare students in Taiwan for study abroad, and we also conduct extensive research on test validity and metrics in partnership with major universities in locales such as the UK, US, Australia, and Hong Kong. On the research front, the LTTC is a regular contributor to the world’s major academic seminars and journals on language testing. We also host conferences to promote exchanges between local and international scholars and share Taiwan’s experiences in the field.
                </p>
                <img src="images/pagEx02-49.svg" alt="" class="bgele">
            </article>
            
            <article class="pagExin02-section04">
                <img src="images/pagEx02-16.png" alt="" class="mlr-20">
                <img src="images/pagEx02-17.png" alt="" class="mlr-20">
                <img src="images/pagEx02-18.png" alt="" class="mlr-20">
                <img src="images/pagEx02-19.png" alt="" class="mlr-20">
            </article>

            <div class="pagExin02-section05">
                <img src="images/pagEx02-50.svg" alt="" class="bgele01">
                <img src="images/pagEx02-50.svg" alt="" class="bgele02">

                <article class="pagExin02-section05--textArea">
                    <img src="images/pagEx02-20.png" alt="" class="mb-30" width="100%">
                    <p class="plr-20 typo-black">
                        The LTTC organized and hosted its first “LTTC International Conference on English Language Teaching and Testing” in 2009
                    </p>
                </article>
                <article class="pagExin02-section05--textArea">
                    <img src="images/pagEx02-21.png" alt="" class="mb-30" width="100%">
                    <p class="plr-20 typo-black">
                        In 2010, the LTTC hosted “The Academic Forum on English Language Testing in Asia” AFELTA International Symposium.
                    </p>
                </article>
                <article class="pagExin02-section05--textArea">
                    <img src="images/pagEx02-22.png" alt="" class="mb-30" width="100%">
                    <p class="plr-20 typo-black">
                        In 2012, former LTTC Director Professor, Tien-En Kao, formally marks a partnership with the University of Southampton in the UK, represented by Professor Mark Spearing, then serving in his capacity as Vice-President of the University.
                    </p>
                </article>
                <article class="pagExin02-section05--textArea">
                    <img src="images/pagEx02-23.png" alt="" class="mb-30" width="100%">
                    <p class="plr-20 typo-black">
                        In 2017, the LTTC hosted the International Conference of the Asian Association for Language Assessment (AALA).
                    </p>
                </article>
            </div>

            <div class="pagExin02-section06 plr-20">
                <h3 class="mb-30">Partner Universities</h3>
                <article class="pagExin02-section06--imgArea plr-20">
                    <img src="images/pagEx02-24.png" alt="" class="">
                    <img src="images/pagEx02-25.png" alt="" class="">
                    <img src="images/pagEx02-26.png" alt="" class="">
                    <img src="images/pagEx02-27.png" alt="" class="">
                </article>
                <article class="pagExin02-section06--imgArea plr-20">
                    <img src="images/pagEx02-28.png" alt="" class="">
                    <img src="images/pagEx02-29.png" alt="" class="">
                    <img src="images/pagEx02-30.png" alt="" class="">
                    <img src="images/pagEx02-48.png" alt="" class="" width="320">
                </article>
            </div>

            <div class="pagExin02-section07 plr-20">
                <h3 class="mb-30">
                    <span>Past Research Publications by the LTTC</span>
                </h3>
                <article class="pagExin02-section07--imgArea owl-custom02 owl-carousel owl-theme">
                    <img src="images/pagEx02-31.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-32.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-33.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-34.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-35.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-36.png" alt="" class="arnorImg">
                    <img src="images/pagEx02-37.png" alt="" class="arnorImg">
                </article>
                <article class="pagExin02-section07--imgArea--big">
                    <img src="images/pagEx02-31.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-32.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-33.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-34.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-35.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-36.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-37.png" alt="" class="mlr-3">
                    <img src="images/pagEx02-52.svg" alt="" class="imgbg">
                </article>
            </div>

            <div class="pagExin02-section08 plr-20">
                <article class="pagExin02-section08--textArea">
                    <img src="images/pagEx02-38.png" alt="" class="pagExin02-section08--textArea--img">
                    <p class="">
                        <!-- <img src="images/pagEx02-38.png" alt="" class="pagExin02-section08--textArea--img"> -->
                        <span class="pagExin02-section08--textArea--text">
                            In 2005, the LTTC research team began a study on linking the  GEPT Reading Test to its CEFR  counterpart.
                            <br />
                            <br />
                            In 2007, the LTTC research team began a comparison study of the GEPT and Cambridge Reading tests at the CEFR B1 and B2 levels.
                        </span>
                    </p>
                </article>
                <article class="pagExin02-section08--textArea">
                    <img src="images/pagEx02-39.png" alt="" class="pagExin02-section08--textArea--img">
                    <p class="">
                        <img src="images/pagEx02-40.png" alt="" class="pagExin02-section08--textArea--logoImg">
                        <br />
                        <span class="pagExin02-section08--textArea--text">
                            In 2012, the LTTC partnered with Lancaster University to conduct a comparison study of the GEPT listening test and its CEFR counterpart.
                        </span>
                    </p>
                </article>
                <article class="pagExin02-section08--textArea">
                    <img src="images/pagEx02-41.png" alt="" class="pagExin02-section08--textArea--img">
                    <p class="">
                        <img src="images/pagEx02-54.png" alt="" class="mb-15 pagExin02-section08--textArea--logoImg--square">
                        <br />
                        <span class="pagExin02-section08--textArea--text">
                            In 2014, the LTTC partnered with the University Of Melbourne to conduct a comparison study of the GEPT writing test and its CEFR counterpart.
                        </span>
                    </p>
                </article>
                <article class="pagExin02-section08--textArea">
                    <img src="images/pagEx02-43.png" alt="" class="pagExin02-section08--textArea--img">
                    <p class="">
                        <img src="images/pagEx02-44.png" alt="" class="mb-15 pagExin02-section08--textArea--logoImg">
                        <br />
                        <span class="pagExin02-section08--textArea--text">
                            In 2015, the LTTC partnered with the University of Bedfordshire to conduct a comparison study of the  GEPT speaking test and its CEFR counterpart.
                        </span>
                    </p>
                </article>
            </div>
        </div>
    </div>

    <div class="pagExin02-sectionGreenBk">
        <div class="max_width">

            <article class="pagExin02-section09 plr-20">
                <h3 class="mb-10">
                    <span>Published Internationally</span>
                </h3>
                <p class="pt-15 typo-black">
                    The tests developed by the LTTC are reputable and credible not only locally in Taiwan but also internationally. The GEPT is currently accepted by the British General Medical Council and 97 universities worldwide as a standard metric for student English proficiency. The LTTC is also widely recognized for language testing in Asia; institutions from Korea, Japan, and Vietnam have reached out to us to gain experience and testing know-how. The LTTC marked a new milestone at the end of 2019 as the editor of <i> English Language Proficiency Testing in Asia</i>, a book published by Routledge. This high-profile publication has furthered Taiwan’s visibility as a leader in language testing. 
                </p>
            </article>

            <div class="pagExin02-section10">
                <h3 class="mb-30 plr-20">
                    <span>Professional Publications</span>
                </h3>
                <section class="pagExin02-section10--textArea">
                    <img src="images/pagEx02-45.png" alt="" class="pagExin02-section10--textArea--img">
                    <article class="pagExin02-section10--textArea--textBk">
                        <p class="pagExin02-section10--textArea--year">2019</p>
                        <p class="">
                            <i>English Language Proficiency Testing in Asia</i>, edited by the LTTC, published by Routledge in 2019.
                        </p>
                    </article>
                </section>

                <section class="pagExin02-section10--textArea pagExin02-section10--textArea--right">
                    <img src="images/pagEx02-46.png" alt="" class="pagExin02-section10--textArea--img">
                    <article class="pagExin02-section10--textArea--textBk textBkLeft">
                        <p class="pagExin02-section10--textArea--year yearleft">2021</p>
                        <p class="">
                            <i>Rethinking EMI Multidisciplinary Perspectives from Chinese-speaking Regions</i>, edited by the LTTC, published by Routledge in 2021.
                        </p>
                    </article>
                </section>
            </div>
        </div>
    </div>
    
    <!-- 首頁底元素 -->
    <div class="pagExBottomEleBk--bgGreen">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     