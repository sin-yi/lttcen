<!-- 電腦版header -->
<header class="patheaderBk js-patheaderBk">
	<a href="index.php" class="patheaderBk-logo" title="">
		<img src="images/logo.svg" alt="語言訓練測驗中心LOGO" class="">
	</a>
	<div class="patheaderBk-linkBk">
		<a href="index.php#milestones-ahref" class="patheaderBk-linkBk--link mr-60">
			Milestones
		</a>
		<a href="index.php#indEx--ahref" class="patheaderBk-linkBk--link mr-60">
			<!-- <span class="ch">線上特展</span>
			<span class="en">e-Exhibits</span> -->
			e-Exhibits
		</a>
		<a href="https://70anniversary.lttc.tw/" class="patheaderBk-linkBk--link">
			中文
		</a>
		
	</div>
</header>
