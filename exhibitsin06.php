<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagExin05">
    
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin06-bannerBk">
        <div class="pagExin06-pageTitBk">
            <h1 class="">
                Educational Service for the Public
            </h1>
        </div>
        <img src="images/pagEx06-02.png" alt="" class="pagExin05-banner">
    </div>
    
    <div class="pagExin06-contentBk">
        <div class="max_width">
            <div class="pagExin06-sectionBk pagExin06-sectionBk--01">
                <h3 class="pagExin05-sectionTit">
                    From Society, for Society
                </h3>
                <p class="pt-25 typo-black">
                    The LTTC would like to thank the people of Taiwan for their trust and affirmation, which enable us to grow from a simple English Training Center under the US aid program to today’s nationally recognized, not-for-profit language training and testing organization. As Taiwan strives towards the goal of becoming a bilingual country by 2030, we at the LTTC will continue to play our part in enhancing Taiwan’s competitiveness by sharing our expertise and resources. 
                </p>
            </div>
            <div class="pagExin06-sectionBk pagExin06-sectionBk--02">
                <!-- <h4 class="typo-tar">
                    勇敢追夢，LTTC一路陪伴相挺
                </h4> -->
                <h5 class="pt-10 pb-20 typo-tar">
                    Follow your dreams…  The LTTC is with you <br />   
                    all the way
                </h5>
                <p class="pt-25 typo-black">
                    On average, 1,500 low-income test takers receive the full GEPT registration fee waiver each year. The LTTC also assists more than 600 people with disabilities to successfully complete the exam every year.
                </p>
                <div class="img01">
                    <img src="images/pagEx06-03.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Fee-waiving for low-income registrants
                    </p>
                </div>
                <div class="img01">
                    <img src="images/pagEx06-04.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Applying for special accommodations 
                        for registrants with disabilities​
                    </p>
                </div>
                <p class="typo-black">
                    To provide children in rural districts with access to better English education, the LTTC organizes training events for teachers in these areas at no cost.
                </p>
                <div class="img02">
                    <img src="images/pagEx06-05.png" alt="" class="" width="100%">
                </div>
                <h3 class="pagExin05-sectionTit">
                    Charitable Events
                </h3>
                <p class="pt-25 typo-black">
                    We listen to and understand the needs of our society. Through academic grants, sponsorships, awards and organized events, we provide opportunities for talents to thrive. It is part of our ongoing commitments to language training and to fostering a positive cycle of learning.
                </p>
            </div>
            <div class="pagExin06-sectionBk pagExin06-sectionBk--03">
                <img src="images/pagEx06-19.svg" alt="" class="bg">
                <p class="pagExin06-enBr em">
                    <!-- <span> -->
                        Academic sponsorship and rewards
                    <!-- </span> -->
                </p>
                <div class="logoBk">
                    <img src="images/pagEx06-06.png" alt="" class="">
                    <img src="images/pagEx06-07.png" alt="" class="">
                    <img src="images/pagEx06-08.png" alt="" class="">
                    <img src="images/pagEx06-09.png" alt="" class="">
                    <img src="images/pagEx06-10.png" alt="" class="">
                    <img src="images/pagEx06-11.png" alt="" class="">
                    <img src="images/pagEx06-12.png" alt="" class="">
                </div>
                <p class="typo-black">
                    Sponsoring large-scale conferences, both domestic and international. 
                </p>
                <div class="img0102Bk">
                    <div class="img01">
                        <img src="images/pagEx06-13.png" alt="" class="" width="100%">
                        <p class="pt-15">
                            Providing a range of prizes to encourage students to strive for excellence.
                        </p>
                    </div>
                    <div class="img02">
                        <img src="images/pagEx06-14.png" alt="" class="" width="100%">
                        <p class="pt-15">
                            Rewarding language-related research, offering grants for research projects and dissertations.
                        </p>
                    </div>
                </div>
            </div>   

        </div>
    </div>
    
    <div class="pagExin06-contentBk pagExin06-contentBk--bgPink">
        <div class="max_width">
            <div class="pagExin06-sectionBk pagExin06-sectionBk--04">
                <img src="images/pagEx06-21.svg" alt="" class="bg01">
                <img src="images/pagEx06-19.svg" alt="" class="bg02">
                <img src="images/pagEx06-20.svg" alt="" class="bg03">

                <p class=" typo-bold em">
                    Hosting competitions
                </p>
                <div class="img01">
                    <img src="images/pagEx06-15.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Encouraging students to achieve a balanced development of language skills through the "English Supermaster“ event in collaboration with Professor Ping-Cheng Yeh and his team.
                    </p>
                </div>
                <div class="img01">
                    <img src="images/pagEx06-16.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Improving verbal communication skills of the younger generation at the speech contest co-organized with the Zhao Lilian Cultural and Educational Foundation.
                    </p>
                </div>
                <br />
                <p class="pagExin06-enBr em">
                    <span>
                        Sponsoring language learning activities on campus
                    </span>
                </p>
                <div class="img01">
                    <img src="images/pagEx06-17.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Sponsoring language-related events in schools, encouraging students to actively participate in international exchange events and use the target language in real-life situations.
                    </p>
                </div>
                <div class="img01">
                    <img src="images/pagEx06-18.png" alt="" class="" width="100%">
                    <p class="pt-10 typo-black">
                        Since 2016, the LTTC has been the exclusive sponsor of the Taiwanese national team at the International Linguistics Olympiad.
                    </p>
                </div>
            </div>
            <!-- <div class="pagExin05-sectionBk--07">
                <img src="images/pagEx05-24.svg" alt="" width="100%" class="bg01">
                <img src="images/pagEx05-22.svg" alt="" width="100%" class="bg02">
                <div class="">
                    <img src="images/pagEx05-19.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        國考新趨勢 英檢證明成入場門票 
                        <br />
                        A new trend in the national examination: English proficiency certificate as a prerequisite 
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx05-20.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        英語檢定萬百種，身在臺灣的你該如何選擇？
                        <br />
                        With so many choices, which English test should Taiwanese take? ​
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx05-21.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        2030雙語國家，兒童英檢怎麼選，看這篇就懂！​
                        <br />
                        Bilingual nation by 2030: Read this to help you decide the best choice of English test for your child​
                    </p>
                </div>
            </div> -->
        </div>
    </div>

    
    <!-- 首頁底元素 -->
    <div class="pagExin06-contentBk--bgPink">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     