<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){
    // 影片打開特效
    // gsap.set("#videoPop01", {
    //     opacity: 0,
    //     scale: 0,
    // });
    // $("#videoOpen01").on('click',function(){
    //     if($("#videoPop01").css("opacity") == "0" ){
    //         gsap.to("#videoPop01", {
    //             opacity: 1,
    //             scale: 1,
    //             zIndex: 10,
    //             duration: 0.5,
    //             ease: "Power2.easeInOut",
    //         });
    //     };
    // });
    // $("#videoPop01").on('click',function(){
    //     gsap.to("#videoPop01", {
    //         opacity: 0,
    //         scale: 0,
    //         zIndex: -9999,
    //     	duration: 0.5,
    //         ease: "Power2.easeInOut",
    //     });
    // });
    // gsap.to(".js-loadingEnd", {
	// 	opacity: 0,
	// 	duration: 1,
	// 	zIndex: -10,
	// 	delay: 1.5,
	// 	// top: 0,
	// 	ease: {ease: Power3.easeInOut, y: 0 },
	// });

});

</script>
<body class="pagExin05">
    
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin05-bannerBk">
        <div class="pagExin05-pageTitBk">
            <h1 class="">
                From Traditional to Digital 
            </h1>
        </div>
        <img src="images/pagEx05-02.png" alt="" class="pagExin05-banner">
    </div>
    
    <div class="pagExin05-contentBk">
        <div class="max_width">
            <div class="pagExin05-sectionBk pagExin05-sectionBk--01">
                <h3 class="pagExin05-sectionTit">
                    <span>Digital Transformation has Brought Us Closer</span>
                </h3>
                <p class="pt-25 typo-black">
                    The LTTC provides online group classes as well as individual ones to enhance scheduling and geographic flexbility, enabling learners who cannot come to the center's physical classrooms to also have the opportunities to pursue interactive language courses.
                </p>
            </div>
            <div class="pagExin05-sectionBk pagExin05-sectionBk--02">
                <h3 class="pagExin05-sectionTit">
                    <span>Digital Transformation of Learning and Assessment</span>
                </h3>
                <p class="pt-25 typo-black">
                    In the past, we accepted without question the knowledge that teachers and textbooks gave us, conditioned to be passive knowledge receivers. Thanks to digital technology, we are now free to surf the Internet and find learning platforms and testing tools that suit us, transforming us into independent learners.
                </p>
                <br />
                <p class="pt-25 typo-bold em">
                    The LTTC has independently developed English and Japanese learning apps, which are widely downloaded and used by language learners in Taiwan.
                </p>
            </div>
            <div class="pagExin05-sectionBk pagExin05-sectionBk--03">
                <div class="img01">
                    <img src="images/pagEx05-03.svg" alt="" width="100%">
                </div>
                <div class="img02">
                    <img src="images/pagEx05-04.svg" alt="" width="100%">
                </div>
            </div>
            <div class="pagExin05-sectionBk pagExin05-sectionBk--04">
                <img src="images/pagEx05-22.svg" alt="" class="bg">
                <div class="column01">
                    <p class="typo-bold em">
                        The LTTC partnered with FUNDAY and LiveABC respectively to create easily accessible, cross-platform English learning tools. 
                    </p>
                    <div class="img01">
                        <img src="images/pagEx05-05.png" alt="" width="100%">
                    </div>
                </div>
                <div class="column02">
                    <p class="typo-bold em">
                        The GEPT has launched an online practice exam service to make online learning easier for test takers.
                    </p>
                    <div class="img02">
                        <img src="images/pagEx05-06.svg" alt="" width="100%">
                    </div>
                </div>
                <div class="column03">
                    <p class="typo-bold em">
                        The LTTC has partnered with PaGamO to develop the “Smart English Competency Alliance (SECA).” SECA educates learners through gaming, helping them develop their English reading competency and learner autonomy.
                    </p>
                    <div class="img03">
                        <img src="images/pagEx05-07.png" alt="" width="100%">
                    </div>
                </div>
                <div class="column04">
                    <p class="typo-bold em">
                        When the LTTC first launched its computerized test center (CBT) in 1996, there were only 52 test takers. Nowadays, around 7,000 candidates per year choose to take the CBT exam. 
                    </p>
                    <div class="img04">
                        <img src="images/pagEx05-08.png" alt="" width="100%">
                    </div>
                </div>
            </div>

            <div class="pagExin05-sectionBk pagExin05-sectionBk--05">
                <img src="images/pagEx05-23.svg" alt="" class="bg01">
                <img src="images/pagEx05-24.svg" alt="" class="bg02">
                <img src="images/pagEx05-22.svg" alt="" class="bg03">
                <h3 class="pagExin05-sectionTit">
                    <span>
                        Getting Closer Through Social Media
                    </span>
                </h3>
                <p class="pt-25 typo-black">
                    Social media has allowed the LTTC to post engaging content on all major platforms, effectively narrowing  the distance between itself and the public, and to tailor content for different user groups. In addition to pictures and text, the LTTC also collaborates with influencers to create educational and entertaining videos. These multiple lively, fun modes of communication showcase the different facets of the LTTC . 
                </p>
                <p class="pt-40 em">
                    On Facebook, the LTTC posts “Learning English with the Editor” (#跟著小編學英語), a series of interesting videos highlighting new vocabulary every week.
                </p>
                <div class="img01">
                    <img src="images/pagEx05-09.png" alt="" width="100%">
                </div>
                <p class="em">
                    On Instagram, current events and popular memes are used to teach English in a relaxed way to catch the eye of the younger generation.
                </p>
                <div class="igImgBk">
                    <img src="images/pagEx05-10.png" alt="" width="100%" class="mb-10">
                    <img src="images/pagEx05-11.png" alt="" width="100%" class="mb-10">
                    <img src="images/pagEx05-12.png" alt="" width="100%" class="mb-10">
                </div>
                <p class="em">
                    LTTC’s official Facebook Messenger & LINE accounts allow real-time interaction so that people can quickly and conveniently contact
                    the center.
                </p>
                <div class="img02">
                    <img src="images/pagEx05-13.png" alt="" width="100%">
                </div>
                <p class="em">
                    Interesting and engaging videos help learners understand the services of the LTTC.
                </p>
                <div class="videoListBk">
                    <div class="videoBk">
                        <!-- <div class="baseVideoBk">
                            <iframe src="https://www.youtube.com/embed/UJGfIy5DSnQ?rel=0&autohide=0&showinfo=0&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <a class="baseImgBk" href="https://www.youtube.com/watch?v=UJGfIy5DSnQ&t=1s" title="Challenging the new GEPT item types" target="_blank">
                            <img src="images/pagEx05-26.png" alt="" class="">
                        </a>
                        <p class="pt-15 pb-20 typo-black">
                            Challenging the new GEPT item types
                        </p>
                    </div>
                    <div class="videoBk">
                        <!-- <div class="baseVideoBk">
                            <iframe src="https://www.youtube.com/embed/DTuPOhTNVO4?rel=0&autohide=0&showinfo=0&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <a class="baseImgBk" href="https://www.youtube.com/watch?v=DTuPOhTNVO4" title="Analyzing the new GEPT item types" target="_blank">
                            <img src="images/pagEx05-27.png" alt="" class="">
                        </a>
                        <p class="pt-15 pb-20 typo-black">
                            Analyzing the new GEPT item types
                        </p>
                    </div>
                    <div class="videoBk">
                        <!-- <div class="baseVideoBk">
                            <iframe src="https://www.youtube.com/embed/eySB8UzefVo?rel=0&autohide=0&showinfo=0&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <a class="baseImgBk" href="https://www.youtube.com/watch?v=eySB8UzefVo" title="Perceptions of GEPT test takers" target="_blank">
                            <img src="images/pagEx05-28.png" alt="" class="">
                        </a>
                        <p class="pt-15 pb-20 typo-black">
                            Perceptions of GEPT test takers
                        </p>
                    </div>
                    <div class="videoBk">
                        <!-- <div class="baseVideoBk">
                            <iframe src="https://www.youtube.com/embed/G0vazIR27aw?rel=0&autohide=0&showinfo=0&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> -->
                        <a class="baseImgBk" href="https://www.youtube.com/watch?v=G0vazIR27aw" title="Unboxing Dr. GEPT​" target="_blank">
                            <img src="images/pagEx05-29.png" alt="" class="">
                        </a>
                        <p class="pt-15 pb-20 typo-black">
                            Unboxing Dr. GEPT​
                        </p>
                    </div>
                </div>
            </div>
            <!-- <p class="pt-25 pagExin-enBr">
                TOEFL(托福)及JLPT(日檢)等國際測驗在臺最久的合作夥伴 ｜ 
                <span>The longest partnership in Taiwan for international brands such as TOEFL & JLPT </span>
            </p> -->            
        </div>
    </div>
    <div class="pagExin05-contentBk pagExin05-contentBk--bgBlue">
        <div class="max_width">
            <div class="pagExin05-sectionBk pagExin05-sectionBk--06">
                <p class="pt-20 typo-bold em">
                    The LTTC publishes a blog providing practical and informational articles to meet the foreign language learning needs of the public.
                </p>
            </div>
            <div class="pagExin05-sectionBk--07">
                <img src="images/pagEx05-24.svg" alt="" width="100%" class="bg01">
                <img src="images/pagEx05-22.svg" alt="" width="100%" class="bg02">
                    <!-- <img src="images/pagEx05-11.png" alt="" width="100%" class="mb-10">
                    <img src="images/pagEx05-12.png" alt="" width="100%" class="mb-10"> -->
                <div class="">
                    <img src="images/pagEx05-19.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        A new trend in the national examination: English proficiency certificate as a prerequisite 
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx05-20.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        With so many choices, which English test should Taiwanese take? ​
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx05-21.png" alt="" class="" width="100%">
                    <p class="pt-20 pb-30 typo-black">
                        Bilingual nation by 2030: Read this to help you decide the best choice of English test for your child​
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- 影片popup -->
    <!-- <div id="videoPop01" class="pagExin05-videoPopBg">
        <div class="videoPositionBk">
            <img src="images/pagEx05-25.svg" alt="" class="videoPopCloseBt" width="20px">
            <div class="baseVideoBk">
                <iframe src="https://www.youtube.com/embed/UJGfIy5DSnQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div> -->

    <!-- <div class="pagExin01-imgBk07">
        <div class="max_width">
            <h3 class="pagExin01-sectionTit">
                最廣為採用的品牌：「全民英檢」(GEPT) |  <br />
                THE MOST POPULAR LANGUAGE EXAM IN TAIWAN
            </h3>
            <p class="pt-25 typo-black">
                國人耳熟能詳的「全民英檢」(GEPT) 2000年推出已邁入第三個十年，累績850萬人次報考，相當於每3個臺灣人就有1人報考GEPT；試題本厚度可堆疊成50座台北101大樓。
                <br />
                <br />
                The GEPT, the most popular foreign language exam in Taiwan, has entered its third decade since launching in 2000. The cumulative total of 8.5 million test takers equates to 1 out of every 3 Taiwanese, while all the past test books stacked one on top of the other would reach a height of 50 Taipei 101 buildings!
            </p>
            <div class="img01">
                <img src="images/pagEx01-12.png" alt="" class="" width="100%">
            </div>
            <div class="img02">
                <img src="images/pagEx01-15.png" alt="" class="" width="100%">
            </div>
        </div>
    </div> -->

    
    <!-- 首頁底元素 -->
    <div class="pagExin05-contentBk--bgBlue">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     