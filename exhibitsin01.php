<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>


<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagExin01">
    
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin01-bannerBk">
        <div class="pagExin01-pageTitBk">
            <h1 class="">
                From OEM to OBM
            </h1>
        </div>
        <img src="images/pagEx01-02.png" alt="" class="pagExin02-banner">
    </div>
    
    <div class="pagExin01-section01Bk">
        <div class="max_width">
            <h3 class="pagExin01-sectionTit"><span>The Partner of Choice for International Brands</span></h3>
            <p class="pt-25 pagExin-enBr em">
                <span>The longest partnership in Taiwan for international brands such as TOEFL & JLPT </span>
            </p>
            <p class="pt-25 typo-black">
                In 1966, the LTTC was entrusted by the US Educational Testing Service (ETS) with administration of the TOEFL exam in Taiwan. Soon after, many international test organizations sought the assistance of the LTTC to administer exams, including the Prometric testing institution, the University of Cambridge in the UK, the Japan International Exchange Foundation, the Japan-Taiwan Exchange Association, the Japan Student Services Organization, the Japanese Government Tourism Bureau, the Korean National Institute of International Education, the Korean Representative Office in Taipei, and the German Language Proficiency Assessment Center. These five decades of professional testing experience explain the esteem in which the LTTC is held by international educational institutions. Moving forward, the LTTC will continue to uphold a judicious and rigorous attitude, cooperate long-term with international partners, and continue to strive for excellence.
            </p>

            <div class=" typo-textAlignCenter">
                <img src="images/pagEx01-14.png" alt="" class="ptb-60 com_width" width="100%" height="auto">
            </div>

            <h3 class="pagExin01-sectionTit">
                <span>Pioneer of Foreign Language Education in Taiwan</span>
            </h3>
            <p class="pt-25 pagExin-enBr em">
                <span>Top choice for government and private organizations</span>
            </p>
            <p class="pt-25 typo-black">
                Founded in 1951, during the US aid period, the center was established to assist the Taiwanese
                government and local industries in preparing trainees for education overseas. As such, it contributed greatly to Taiwan’s fledgling international presence. In 1965, the institution was renamed “The Language Center,” and the scope of its language program expanded to cover English, Japanese, French, German, and Spanish. The target audience was also expanded to include both the private sector and the general public. In 1979, the institution was renamed " The Language Training & Testing Center (LTTC)."
            </p>

            <div class="pagExin01-imgBk01">
                <div class="">
                    <img src="images/pagEx01-03.png" alt="" class="" width="100%" height="auto">
                    <p class="pt-10 pb-40">
                        In 1965, the institution was renamed"The Language Center".
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx01-04.png" alt="" class="" width="100%" height="auto">
                    <p class="pt-10 pb-40">
                        In 1985, the LTTC relocated into the Language Building of National Taiwan University.
                    </p>
                </div>
            </div>

            <h3 class="pagExin01-sectionTit">
                <br> Development of Language Courses and Teaching Materials in Response to Social and Economic Needs
            </h3>
            <p class="pt-25 pagExin-enBr em">
                <span> State of the art training and teaching materials to serve learners of all stages </span>
            </p>
            <p class="pt-25 typo-black">
                For the past 70 years, the LTTC has been deeply engaged in language training. The center is an important driving force behind the increase in learning motivation and overall foreign language proficiency in Taiwan. The LTTC tailors its services to Taiwan's societal development, and it pursues both research and teaching while striving to meet the needs of learners. As the training institution of choice for hundreds of organizations, the LTTC is recognized and trusted by the government and private sectors alike. In addition, the center has developed a wealth of highly regarded teaching and learning resources for teachers and learners. Furthermore, it provides a variety of learning channels, including apps, online games, and online courses, in line with current learning trends.
            </p>

            <div class="pagExin01-imgBk02">
                <img src="images/pagEx01-17.svg" alt="" class="bg">
                <div class="">
                    <img src="images/pagEx01-05.png" alt="" class="" width="100%" height="auto">
                    <p class="pt-10 pb-40">
                        The first trainees utilizing turntables for language learning
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx01-06.png" alt="" class="" width="100%" height="auto">
                    <p class="pt-10 pb-40">
                        Early training materials developed by LTTC
                    </p>
                </div>
                <div class="">
                    <img src="images/pagEx01-07.png" alt="" class="" width="100%" height="auto">
                    <p class="pt-10 pb-40">
                        Vintage casette player for listening comprehension practice
                    </p>
                </div>
            </div>

            <div class="pagExin01-imgBk03">
                <div class="sml pb-10">
                    <img src="images/pagEx01-08.png" alt="" class="" width="" height="auto">
                </div>   
                <div class="">
                    <img src="images/pagEx01-09.png" alt="" class="" width="100%" height="auto">
                </div>
                <div class="clear"></div>  
                <p class="pt-20 pb-40">
                    Professional recording & photo studio for recording and shooting digital courses and exam questions.
                </p>
            </div>

            <div class="pagExin01-imgBk04">
                <img src="images/pagEx01-18.svg" alt="" class="bg">
                <img src="images/pagEx01-19.svg" alt="" class="bg02">
                <img src="images/pagEx01-10.png" alt="" class="pb-10" width="100%" height="auto">
                <div class="sml">
                    <img src="images/pagEx01-11.png" alt="" class="" width="100%" height="auto">
                </div>
                <p class="pt-10 pb-40">
                    Automated large printing press for printing teaching and testing resources.
                </p>
            </div>

            <div class="pagExin01-imgBk05">
                <h3 class="pagExin01-sectionTit">
                    Foreign Language Tests Developed & Made in Taiwan
                </h3>
                <p class="pt-25 pagExin-enBr em">
                    <span>Tests for five foreign languages, tailored for Taiwan, developed for different levels</span>
                </p>
                <p class="pt-25 typo-black">
                    The Foreign Language Proficiency Test (FLPT) was developed in 1965. Encompassing English, Japanese, French, German, and Spanish, the FLPT became the first exam in Taiwan offered in five languages. Since its inception, the FLPT has played an important role in the internationalization of Taiwanese industry. Notable uses include the following: the Tourism Bureau launching a foreign language test for tour guides, the ROC Foreign Trade Development Association conducting a "New Personnel Language Exam,”the French–German–Spanish Test for Studying Abroad, and finally the English Proficiency Test for Elementary School Teachers, entrusted to the FLPT by the Ministry of Education. The success of the FLPT led to the refinement and diversification of the testing services offered by the LTTC. Soon after, tests were developed to take into account both internationalization and localization, such as the College Student English Proficiency Test (CSEPT), the General English Proficiency Test (GEPT), GEPT Kids, and the Second Foreign Language Proficiency Test-Basic (SFLPT) to test the learning achievements of beginning learners in Japanese, French, German and Spanish. The growth in test takers from an initial 10,000 individuals to the recent high of 700,000 per year clearly demonstrates that LTTC exams are among the most popular and trusted in Taiwan. 
                </p>
            </div>

            <div class="pagExin01-imgBk06">
                <img src="images/pagEx01-17.svg" alt="" class="bg">
                <!-- <p class="chartTit typo-black">
                    <span class="color color--yellow"></span>
                    Self-developed Tests
                </p>
                <p class="chartTit typo-black">
                    <span class="color color--blue"></span>
                    Agents for international tests
                </p> -->
                <div class="img01">
                    <img src="images/pagEx01-13.png" alt="" class="" width="100%">
                </div>
                <div class="img02">
                    <img src="images/pagEx01-16.png" alt="" class="" width="100%">
                </div>
                <p class="typo-black">
                    From international tests to self-developed MIT tests: the number of international tests remains steady, while the number of test takers for MIT tests increased significantly after the launch of the GEPT.
                </p>
            </div>
        </div>
    </div>

    <div class="pagExin01-imgBk07">
        <div class="max_width">
            <h3 class="pagExin01-sectionTit">
                THE MOST POPULAR LANGUAGE EXAM IN TAIWAN
            </h3>
            <p class="pt-25 typo-black">
                The GEPT, the most popular foreign language exam in Taiwan, has entered its third decade since launching in 2000. The cumulative total of 8.5 million test takers equates to 1 out of every 3 Taiwanese, while all the past test books stacked one on top of the other would reach a height of 50 Taipei 101 buildings!
            </p>
            <h6 class="pagExin01-imgBk07--emText">
                All the past test books stacked one on top of the other would reach a height of 50 Taipei 101 buildings.
            </h6>
            <div class="img01">
                <img src="images/pagEx01-12.png" alt="" class="" width="100%">
            </div>
            <div class="img02">
                <img src="images/pagEx01-15.png" alt="" class="" width="100%">
            </div>
        </div>
    </div>

    
    <!-- 首頁底元素 -->
    <div class="pagExBottomEleBk--bgBlue">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     