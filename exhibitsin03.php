<!DOCTYPE html>	
<head>
<title>LTTC 70th Anniversary Website</title>

<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" /> -->
<!-- 抓banner圖 -->
<!-- <meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" /> -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image:width" content="" />
<meta property="og:image:height" content="" /> -->

<?php require('head.php') ?>
<!-- 輪播 -->
<!-- <link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="vendor/Owl/owl.theme.default.css">
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1280: {
                items: 2
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        loop: true,
        margin: 3,
        stagePadding:0,
        smartSpeed:450,
        dots: false,
        nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768: {
                items: 3
            },
        }
    });
    
});
</script> -->
<script language="javascript">

// 動畫效果
$(document).ready(function() { 

    gsap.registerPlugin(ScrollTrigger);
    ScrollTrigger.matchMedia({
    // desktop
    "(min-width: 1440px)": function() {

      
    },
  
    // mobile
    "(max-width: 768px)": function() {
       
    },
      
    // all 
    "all": function() {
          
    },
  }); 
});

$(window).on('load',function(){

});

</script>
<body class="pagExin03">
    <div class="pagExin04--bg">
        <img src="images/pagEx03-48.svg" alt="" class="pagExin03--bg01">
        <img src="images/pagEx03-49.svg" alt="" class="pagExin03--bg02">
        <img src="images/pagEx03-50.svg" alt="" class="pagExin03--bg03">
        <img src="images/pagEx03-48.svg" alt="" class="pagExin03--bg04">
    </div>
    
    <?php // require('loading.php') ?>

    <?php require('smlNav.php') ?>
    <?php require('header.php') ?>

    <div class="pagExin03-bannerBk">
        <div class="pagExin03-pageTitBk">
            <h1 class="">
                <span>
                    Towards Diversified Services
                </span>
            </h1>
        </div>
        <img src="images/pagEx03-02.png" alt="" class="pagExin03-banner">
    </div>
    
    <div class="pagExin03-contentBk">
        <div class="max_width">
            <div class="pagExin03-sectionBk pagExin03-sectionBk--01">
                <h3 class="pagExin03-sectionTit">
                    Providing Diversified Innovative Services to Meet the Changing Needs of the Society
                </h3>
                <p class="em ptb-30">
                    From English to second foreign languages, from daily lives to professional careers, the LTTC helps learners of different generations to improve foreign language abilities
                </p>
                <p class="typo-black">
                    When the center was first founded, it focused primarily on English training. As learners' interest in languages and cultures from places such as Europe and Japan increased, the LTTC expanded its training and assessment services to six languages. Course content came to include everyday and business language and extended into professional training areas such as social interaction, business communication, negotiation, presentation, and interpretation. At the same time, in accordance with the extension of foreign language teaching into the elementary school system and the implementation of the competency curriculum, the center planned and designed courses and assessments focused on primary and secondary school students, emphasizing the cultivation of self-learning strategies. 
                    <br>
                    <br>
                    The LTTC’s language training service includes multiple languages. Training emphasizes real communication scenarios  and the cultivation of cultural understanding.
                </p>
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--02">
                <div class="imgBk-0102">
                    <div class="img01">
                        <img src="images/pagEx03-03.png" alt="" class="" width="100%">
                        <p class="">
                            European language learning resources
                        </p>
                    </div>
                    <div class="img02">
                        <img src="images/pagEx03-04.png" alt="" class="" width="100%">
                        <p class="pt-10 typo-black">
                            Japanese cultural experience event
                        </p>
                    </div>
                </div>
                <div class="imgBk-03">
                    <div class="img03">
                        <img src="images/pagEx03-48--big.png" alt="" class="" width="100%">
                        <p class="pt-20 typo-black">
                            Breakdown of Courses Currently Offered: English, 60%; Japanese, 30%; Other, 10%.
                        </p>
                    </div>
                    <p class="typo-black img03--con">
                        In 1951,  the center provided English training for trainees heading to the US
                        <br><br>
                        With government funding supporting training in Europe and Japan, the scope of languages was expanded from English only to include Japanese, French, German, and Spanish.
                        <br><br>
                        In 2011, Korean was added to accommodate demand.
                    </p>
                </div>
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--03">
                <h3 class="pagExin03-sectionTit">
                    Trusted by the public sector, with years of experience providing specialized language training programs.
                </h3>
                <div class="logoBk">
                    <img src="images/pagEx03-05.png" alt="" class="logo">
                    <img src="images/pagEx03-06.png" alt="" class="logo">
                    <img src="images/pagEx03-07.png" alt="" class="logo">
                    <img src="images/pagEx03-08.png" alt="" class="logo">
                    <img src="images/pagEx03-09.png" alt="" class="logo">
                    <img src="images/pagEx03-10.png" alt="" class="logo">
                    <img src="images/pagEx03-11.png" alt="" class="logo">
                    <img src="images/pagEx03-12.png" alt="" class="logo">
                    <img src="images/pagEx03-13.png" alt="" class="logo">
                    <img src="images/pagEx03-14.png" alt="" class="logo">
                    <img src="images/pagEx03-15.png" alt="" class="logo">
                    <img src="images/pagEx03-16.png" alt="" class="logo">
                </div>
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--03">
                <h3 class="pagExin03-sectionTit">
                    Organizing language training programs for private enterprise.
                </h3>
                <div class="logoBk">
                    <img src="images/pagEx03-17.png" alt="" class="logo">
                    <img src="images/pagEx03-18.png" alt="" class="logo">
                    <img src="images/pagEx03-19.png" alt="" class="logo">
                    <img src="images/pagEx03-20.png" alt="" class="logo">
                    <img src="images/pagEx03-21.png" alt="" class="logo">
                    <img src="images/pagEx03-22.png" alt="" class="logo">
                    <img src="images/pagEx03-23.png" alt="" class="logo">
                    <img src="images/pagEx03-24.png" alt="" class="logo">
                    <img src="images/pagEx03-25.png" alt="" class="logo">
                    <img src="images/pagEx03-26.png" alt="" class="logo">
                    <img src="images/pagEx03-27.png" alt="" class="logo">
                    <img src="images/pagEx03-28.png" alt="" class="logo">
                    <img src="images/pagEx03-29.png" alt="" class="logo">
                    <img src="images/pagEx03-30.png" alt="" class="logo">
                    <img src="images/pagEx03-31.png" alt="" class="logo">
                    <img src="images/pagEx03-32.png" alt="" class="logo">
                    <img src="images/pagEx03-33.png" alt="" class="logo">
                    <img src="images/pagEx03-34.png" alt="" class="logo">
                    <img src="images/pagEx03-35.png" alt="" class="logo">
                    <img src="images/pagEx03-36.png" alt="" class="logo">
                    <img src="images/pagEx03-37.png" alt="" class="logo">
                    <img src="images/pagEx03-38.png" alt="" class="logo">
                    <img src="images/pagEx03-39.png" alt="" class="logo">
                    <img src="images/pagEx03-40.png" alt="" class="logo">
                </div>
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--04">
                <p class="typo-black">
                    Foreign language education in Taiwan has been extended to elementary and middle school students. In line with this trend, the LTTC has launched GEPT Kids, the Second Foreign Language Proficiency Test, as well as language training for middle school students.
                </p>
                <div class="imgBk-0102">
                    <div class="img01">
                        <img src="images/pagEx03-41.png" alt="" class="" width="100%">
                        <p class="typo-black pt-10">
                            The LTTC was commissioned by the Department of Education, Taipei City Government to host The Second Foreign Language Competition for High School Students.
                        </p>
                    </div>   
                    <div class="img02">
                        <img src="images/pagEx03-42.png" alt="" class="" width="100%">
                        <p class="typo-black">
                            GEPT Kids 1 on 1 speaking test
                        </p>
                    </div>   
                </div> 
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--05">
                <p class="typo-black">
                    Responding to the Curriculum Guidelines for the 12-year Basic Education and a move towards competency-oriented assessment, the LTTC and PaGamO have partnered to launch an online English reading course, which has been adopted by the K-12 Education Administration, MOE.
                </p>
                <div class="img01">
                    <img src="images/pagEx03-43.png" alt="" class="" width="100%">
                    <p class="typo-black ">
                        Left to right: Professor Vincent W. Chang, convener of 12-year compulsory English education; Professor Ping-Cheng Yeh, National Taiwan University; Professor Tung Shen, the Executive Director of the LTTC; Dr. Jessica Wu, the Research and Development Program Director of the LTTC
                    </p>
                </div>   
            </div>
        </div>
    </div>

    
    <div class="pagExin03-contentBk pagExin03-contentBk--bgYellow">
        <div class="max_width">
            <div class="pagExin03-sectionBk pagExin03-sectionBk--06">
                <h3 class="pagExin03-sectionTit">
                    Keeping Up with the Times on Our Path to Becoming a Bilingual Nation
                </h3>
                <p class="em ptb-20">
                    A powerful partner for schools, public service units, and enterprises at all levels in the move towards internationalization and bilingualization   
                </p>
                <p class="typo-black">
                    The LTTC combines teaching and evaluation; incorporates both R&D and practical experience; actively responds to national, county and city government policies; and conducts both research into enhancing the abilities of bilingual teachers at elementary and middle schools and the development of assessment resources for the bilingual classroom. Organizing customized, communication-oriented language training courses, the LTTC responds to the increasing demands of various agencies for foreign affairs and international exchange, such as the Ministry of Foreign Affairs, the Ministry of Health and Welfare, the Ministry of Economics, the Ministry of National Defense, the Civil Service Academy, and hundreds of other enterprises and institutions.
                </p>
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--07">
                <h3 class="pagExin03-sectionTit">
                    Providing assessment services in response to national English education policies
                </h3>
                <div class="img01">
                    <img src="images/pagEx03-44.png" alt="" class="" width="100%">
                    <p class="typo-black pt-20">
                        In line with the 2001 English education initiative, the LTTC was commissioned to administer a test assessing the language proficiency of elementary school English teachers, starting in 1999. Those who passed the test and completed the teacher training would then be qualified to teach English at elementary schools. 
                        The MOE estimated the number of teachers needed in 2001 would be about 3,300. However, the actual number of registrants fort the test was approximately 50,000, far exceeding expectations.
                    </p>
                </div> 
                <div class="img02">
                    <img src="images/pagEx03-45.png" alt="" class="" width="100%">
                    <p class="typo-black pt-20">
                        To gain a clearer understanding of the level of English among technical and vocational school students, the LTTC was commissioned by the MOE to conduct a 3-year longitudinal study (2001 to 2003) to examine and track English proficiency in this group. Over the 3 years, a total of 28,000 students participated in the study.
                    </p>
                </div>  
            </div>
            <div class="pagExin03-sectionBk pagExin03-sectionBk--08">
                <p class="typo-black pt-20">
                    The LTTC signed a letter of intent for cooperation with the Department of Education, Taipei City Government, to jointly promote the implementation of bilingual experimental courses by offering assessment resources and teacher training programs.
                </p>
                <div class="imgBk-0102">
                    <div class="img01">
                        <img src="images/pagEx03-46.png" alt="" class="" width="100%">
                        <p class="typo-black pt-10">
                            Signing a letter of intent for cooperation
                        </p>
                    </div>   
                    <div class="img02">
                        <img src="images/pagEx03-47.png" alt="" class="" width="100%">
                        <p class="typo-black pt-10">
                            Teacher training workshops for junior-high and elementary school teachers of Taipei City
                        </p>
                    </div>   
                </div> 
            </div>
        </div>
    </div>

    
    <!-- 首頁底元素 -->
    <div class="pagExin03-contentBk--bgYellow">
        <div class="pagExBottomEleBk">
            <img src="images/pagele-02.png" alt="綠星" class="pagExBottomEle01">
            <img src="images/indele08.png" alt="黃星" class="pagExBottomEle02">
            <img src="images/indele07.png" alt="紅星" class="pagExBottomEle03">
            <img src="images/indbanner05.png" alt="女孩" class="pagExBottomEle04">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="pagExBottomEle05" target="_blank">
                <img src="images/pagEx02-47.png" alt="有獎徵答按鈕" class="" width="100%">
            </a>
        </div>
    </div>
    

    <!-- 回頁頂 -->
    <a href="javascript:void(0);" class="modTopBtBk">
        <img src="images/back-top.svg" alt="回頁頂" class="modTopBt">
    </a>
</body>
</html>

     