<!-- 小視口全域導覽列 -->
<header class="patSmlHeader">
	<a href="index.php" class="patSmlHeader-logo" title="回到「語言訓練測驗中心」網站">
		<img src="images/logo.svg" alt="語言訓練測驗中心LOGO" class="">
	</a>
	<div class="patSmlHeader-navbt jsNavSml-bt" tabindex="-1" title="打開網站導覽列">
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon01"></div>
		<div class="patSmlHeader-navbt--icon patSmlHeader-navbt--icon02"></div>
	</div>
</header>


<!-- 小視口全域導覽列開合區 -->
<div class="patSmlNav jsNavSmall">
	<!-- 全域導覽列連結 -->
	<ul class="patLevelArea jsAccording">
		<a href="index.php#milestones-ahref" class="patLevelArea-firstLink jsAccording-firstLik" title="Milestones" tabindex="-1" data-i18n="common_news">
			Milestones
		</a>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="index.php#indEx--ahref" class="patLevelArea-firstLink jsAccording-firstLik" title="e-Exhibits" tabindex="-1" data-i18n="common_about_us">
			e-Exhibits
		</a>
		<div class="clear"></div>
	</ul>
	<ul class="patLevelArea jsAccording">
		<a href="https://70anniversary.lttc.tw/" class="patLevelArea-firstLink jsAccording-firstLik" title="e-Exhibits" tabindex="-1" data-i18n="common_about_us">
			中文
		</a>
		<div class="clear"></div>
	</ul>
</div>

